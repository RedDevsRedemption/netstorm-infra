﻿using NetStorm.Classes.Devices;
using NetStorm.Classes.Lists;
using Microsoft.AspNetCore.Mvc;

namespace NetStorm.Controllers
{
	[Route("api/devices")]
	class DeviceController : Controller
	{
		[HttpGet]
		public IActionResult GetDevices([FromQuery(Name = "device")]string host = "")
		{
			if(host == "")
				return Ok(DeviceList.Global.GetEnumerator());

			INetworkDevice device = NetworkDevice.Empty;
			foreach (var d in DeviceList.Global)
				if (d.IPv4 == host || d.IPv6 == host || d.Hostname == host)
				{
					device = d;
					break;
				}

			return Ok(device);
		}
	}
}
