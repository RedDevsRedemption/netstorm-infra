﻿using Microsoft.AspNetCore.Mvc;

namespace NetStorm.Controllers
{
	[Route("api/interfaces")]
	public class InterfacesController : Controller
	{
		[HttpGet]
		public IActionResult GetInterfaces()
		{
			return Ok(Core.Instance.Interfaces);
		}

		//POST: api/interfaces/{id}?action=start/stop
		[HttpPost("{id}")]
		public bool StartNIC(int id, [FromQuery(Name = "action")]string action = "false")
		{
			var b = action == "start";

			if (b)
				return Core.Instance.SelectNIC(id);
			else
			{
				Core.Instance.StopInterface();
				return true;
			}
		}
	}
}