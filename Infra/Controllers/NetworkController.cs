﻿using Microsoft.AspNetCore.Mvc;
using NetStorm.Modules;
using NetStorm.Modules.HostDiscovery;

namespace NetStorm.Controllers
{
	[Route("api/network")]
	public class NetworkController : Controller
	{
		[HttpGet("overview")]
		public IActionResult Get()
		{
			if (Core.Instance.CurrentInterface == null)
				return BadRequest();

			return Ok(Core.Instance.CurrentInterface.Network);
		}

		[HttpPost]
		public IActionResult Post([FromQuery(Name = "action")]string action = "abort", [FromBody]NetworkScannerOptions ops = null)
		{
			if (Core.Instance.CurrentInterface == null)
				return BadRequest();

			var nw = Core.Instance.CurrentInterface.Network;
			var scanner = StaticModuleCollection.NetworkScanner;
			if (ops != null)
			{
				Logger.Log.Debug($"Received ops {ops.ToString()}");
				scanner.RealtimeScan = ops.RealtimeScan;
				scanner.UseThreads = ops.UseThreads;

				if (ops.Fingerprint)
					scanner.AddModule(new PingFingerprinter());
				else
					scanner.RemoveModule(typeof(PingFingerprinter));

				if (ops.ResolveHostnames)
					scanner.AddModule(new DnsResolver());
				else
					scanner.RemoveModule(typeof(DnsResolver));
			}

			var successful = true;
			var start = action == "scan";

			if (start)
				successful = scanner.Start(nw);
			else
				scanner.Abort();

			return successful ? NoContent() : StatusCode(503);
		}

		public class NetworkScannerOptions
		{
			public bool RealtimeScan { get; set; }
			public bool UseThreads { get; set; }
			public bool ResolveHostnames { get; set; }
			public bool Fingerprint { get; set; }

			public override string ToString()
			{
				return $"RT: {RealtimeScan}, UseThreads: {UseThreads}, ResolveHostnames {ResolveHostnames}, Fingerprint {Fingerprint}";
			}
		}
	}
}
