﻿using System.Collections.Generic;
using NetStorm.Classes;
using NetStorm.Classes.Devices;
using NetStorm.Modules.Portscanner;
using Microsoft.AspNetCore.Mvc;

namespace NetStorm.Controllers
{
	[Route("api/portscanner")]
	public class PortscanController : Controller
	{
		private static readonly IPortScanner portScanner = BasePortScanner.GetEnvironmentPortScanner();

		[HttpGet("target")]
		public IActionResult Get()
		{
			return Ok(portScanner.Target);
		}

		[HttpPost]
		public IActionResult Post([FromQuery(Name = "action")]string action = "abort", [FromBody]PortScannerOptions ops = null)
		{
			if (ops == null)
				return BadRequest();
			
			portScanner.ScanMode = ops.Mode;
			portScanner.UseThreads = ops.UseThreads;
			portScanner.GrabBanners = ops.GrabBanners;

			var successful = true;
			var start = action == "scan";

			if (start)
			{
				portScanner.SetTarget(new NetworkDevice(ops.Host), ops.Ports);
				successful = portScanner.Start();
			}
			else
				portScanner.Abort();

			return successful ? NoContent() : StatusCode(503);
		}

		public class PortScannerOptions
		{
			public ushort[] Ports { get; set; }
			public string Host { get; set; } = "";
			public bool UseThreads { get; set; } = true;
			public bool GrabBanners { get; set; } = false;
			public PortscanMode Mode { get; set; } = PortscanMode.Syn;

			public override string ToString()
			{
				return $"Host: {Host}, UseThreads: {UseThreads}, GrabBanners {GrabBanners}, Mode {Mode}";
			}
		}
	}
}
