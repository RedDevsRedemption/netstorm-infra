﻿using NetStorm.Classes.Devices;
using NetStorm.Classes.Lists;
using NetStorm.Classes.NetworkTools;
using NetStorm.Classes.Utils;
using NetStorm.Logger;
using System;
using System.ArrayExtensions;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace NetStorm
{
	sealed class Core
	{
		private const string deviceSeperator = @"\Device\NPF_";

		public static Core Instance { get; } = new Core();

		public INetworkInterface CurrentInterface { get; private set; }
		public IReadOnlyCollection<INetworkInterface> Interfaces => interfaces.AsReadOnly();

		public event EventHandler OnShutdown, InterfacesChanged;
		public event EventHandler<INetworkInterface> InterfaceStopped, InterfaceStarted;

		private readonly List<INetworkInterface> interfaces;

		private Core()
		{
			interfaces = new List<INetworkInterface>();
			AppDomain.CurrentDomain.UnhandledException += (_, __) => Log.Critical(__.ExceptionObject.ToString());
			NetworkChange.NetworkAddressChanged += NetworkChange_NetworkAddressChanged;
		}

		~Core()
		{
			NetworkChange.NetworkAddressChanged -= NetworkChange_NetworkAddressChanged;
			CurrentInterface?.StopCapture();
			AppDomain.CurrentDomain.UnhandledException -= (_, __) => Log.Critical(__.ExceptionObject.ToString());
		}

		/// <summary>
		/// Stop packet capture on all devices and exit the app
		/// </summary>
		public void Shutdown()
		{
			Log.Normal("Closing nics and exit...");
			foreach (var nic in interfaces)
				nic.StopCapture();

			OnShutdown?.Invoke(this, new EventArgs());
		}

		/// <summary>
		/// Stop the current interface
		/// </summary>
		public void StopInterface()
		{
			CurrentInterface?.StopCapture();
			CurrentInterface = null;
		}

		/// <summary>
		/// Select and start capture on a specific NIC
		/// </summary>
		/// <param name="index"></param>
		/// <returns></returns>
		public bool SelectNIC(int index)
		{
			if (index < 0 || index >= Interfaces.Count)
				return false;

			if (CurrentInterface != null && Interfaces.ElementAt(index).Equals(CurrentInterface) && CurrentInterface.IsCaputring)
				return false;

			CurrentInterface?.StopCapture();

			try
			{
				var nic = Interfaces.ElementAt(index);
				Log.Normal($"Attemting to start nic: {nic.InterfaceName}");
				StartNIC(nic);
				return true;
			}
			catch (Exception e)
			{
				Log.Error("Unable to start nic: " + e.Message);
				return false;
			}
		}

		/// <summary>
		/// Start packet capture on a specific NIC
		/// </summary>
		/// <param name="index"></param>
		private void StartNIC(INetworkInterface nic)
		{
			CurrentInterface = nic;

			DeviceList.Global.Clear();

			CurrentInterface.CaptureStopped += CurrentInterface_CaptureStopped;
			CurrentInterface.CaptureStarted += CurrentInterface_CaptureStarted;
			CurrentInterface.StartCapture();
		}

		private void CurrentInterface_CaptureStarted(object sender, EventArgs e)
		{
			InterfaceStarted?.Invoke(this, CurrentInterface);
		}

		/// <summary>
		/// Register and handle NIC changes
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void NetworkChange_NetworkAddressChanged(object sender, EventArgs e)
		{
			NetworkChange.NetworkAddressChanged -= NetworkChange_NetworkAddressChanged;
			Log.Debug("NIC change registered...");

			//Check the need for a refresh
			var refresh = false;
			var newNics = GetNetworkInterfaces();
			if (interfaces.Count == newNics.Count)
			{
				foreach (var nic in interfaces)
					if (!newNics.Contains(nic))
					{
						refresh = true;
						break;
					}
			}
			else refresh = true;

			//Update nic list
			if (refresh)
			{
				Log.Normal("Updating nics");

				var oldNic = CurrentInterface;
				RefreshNICList();
				if (Interfaces.Contains(oldNic))
					SelectNIC(interfaces.IndexOf(oldNic));
			}

			NetworkChange.NetworkAddressChanged += NetworkChange_NetworkAddressChanged;
		}

		/// <summary>
		/// Fires, if an interface stopps capturing
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void CurrentInterface_CaptureStopped(object sender, EventArgs e)
		{
			CurrentInterface.CaptureStarted -= CurrentInterface_CaptureStarted;
			CurrentInterface.CaptureStopped -= CurrentInterface_CaptureStopped;
			InterfaceStopped?.Invoke(this, CurrentInterface);
		}

		/// <summary>
		/// Refresh NIC list
		/// </summary>
		public void RefreshNICList()
		{
			lock (interfaces)
			{
				CurrentInterface?.StopCapture();
				interfaces.Clear();
				DeviceList.Global.ClearPrivate();
				interfaces.AddRange(GetNetworkInterfaces());
				CurrentInterface = null;
				LogInterfaces();
			}
			InterfacesChanged?.Invoke(this, new EventArgs());
		}

		private void LogInterfaces()
		{
			foreach (var nic in Interfaces)
				Log.Normal($"{nic.InterfaceName};" +
					$" IPv4: {nic.IPv4};" +
					$" IPv6: {nic.IPv6}; " +
					$" Network: {nic.Network.IPNetwork.ToString()};" +
					$" MAC: {nic.MAC.ToCannonicalString()} ({nic.MACVendor.Manufacturer})"
				);
		}

		/// <summary>
		/// List available NICs
		/// </summary>
		/// <returns></returns>
		private List<INetworkInterface> GetNetworkInterfaces()
		{
			var list = ListNICsNative();
			switch (Config.Instance.NetworkAPI)
			{
				case NetworkAPI.LibPcap:
					list = ListLibPCAPNics(list);
					break;

				case NetworkAPI.WinPcap:
					list = ListWinPCAPNics(list);
					break;

			}
			return list;
		}

		/// <summary>
		/// Return a list of available WinPCAP NICs
		/// </summary>
		/// <param name="nicList"></param>
		/// <returns></returns>
		private List<INetworkInterface> ListWinPCAPNics(List<INetworkInterface> nicList)
		{
			List<INetworkInterface> networkInterfaces = new List<INetworkInterface>(nicList.Count);

			try
			{
				foreach (var pcapNic in SharpPcap.WinPcap.WinPcapDeviceList.New())
				{
					foreach (var nic in nicList)
					{
						if (pcapNic.Interface.Name.Split(deviceSeperator)[1] == nic.ID)
						{
							networkInterfaces.Add(new PcapNetworkInterface(pcapNic, nic));
							break;
						}
					}
				}
			}
			catch (Exception e)
			{
				Log.Error($"Cannot load pcap interfaces: {e.Message}");
			}

			return networkInterfaces;
		}

		/// <summary>
		/// Return a list of available LibPCAP NICs
		/// </summary>
		/// <param name="nicList"></param>
		/// <returns></returns>
		private List<INetworkInterface> ListLibPCAPNics(List<INetworkInterface> nicList)
		{
			List<INetworkInterface> networkInterfaces = new List<INetworkInterface>(nicList.Count);

			try
			{
				foreach (var pcapNic in SharpPcap.LibPcap.LibPcapLiveDeviceList.New())
				{
					foreach (var nic in nicList)
					{
						if (pcapNic.Interface.Name.Split(deviceSeperator)[1] == nic.ID)
						{
							networkInterfaces.Add(new PcapNetworkInterface(pcapNic, nic));
							break;
						}
					}
				}
			}
			catch (Exception e)
			{
				Log.Error($"Cannot load pcap interfaces: {e.Message}");
			}
			return networkInterfaces;
		}

		/// <summary>
		/// Return a list of available NICs
		/// </summary>
		/// <param name="nicList"></param>
		/// <returns></returns>
		private List<INetworkInterface> ListNICsNative()
		{
			string[] excludedIPs = new string[] { "255.255.255.255", "127.0.0.1", "0.0.0.0" };

			IPInterfaceProperties ipProperties = null;

			INetworkDevice[] arpTable = ArpCache.GetEntries();

			var devices = new List<INetworkInterface>();

			foreach (var nic in System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces())
			{
				ipProperties = nic.GetIPProperties();

				if (nic.OperationalStatus != OperationalStatus.Up || ipProperties.GatewayAddresses.Count <= 0 || !nic.Supports(NetworkInterfaceComponent.IPv4))
					continue;

				string ipv4 = "";
				string ipv6 = "";
				string ipv4Pref = "";

				//NIC lookup
				foreach (var unicastAddr in ipProperties.UnicastAddresses)
				{
					if (unicastAddr.Address.AddressFamily == AddressFamily.InterNetwork)
					{
						ipv4 = unicastAddr.Address.ToString();
						ipv4Pref = unicastAddr.IPv4Mask.ToString();
					}
					else if (unicastAddr.Address.AddressFamily == AddressFamily.InterNetworkV6)
					{
						ipv6 = unicastAddr.Address.ToString();
					}
				}

				if (excludedIPs.Contains(ipv4) || ipv4 == "")
					continue;

				var gateway = ResolveGateway(ipProperties.GatewayAddresses, arpTable);

				if (gateway == null)
					continue;

				devices.Add(new Classes.Devices.NetworkInterface(
					nic.Id,
					ResolveGateway(ipProperties.GatewayAddresses, arpTable),
					nic.GetPhysicalAddress(),
					nic.Name,
					ipv4,
					ipv4Pref,
					ipv6)
				);
			}
			return devices;
		}

		/// <summary>
		/// Resolve the NICs attached gateway
		/// </summary>
		/// <param name="gatewayIPAddressInformation"></param>
		/// <param name="arpTable"></param>
		/// <returns></returns>
		private Gateway ResolveGateway(GatewayIPAddressInformationCollection gatewayIPAddressInformation, INetworkDevice[] arpTable)
		{
			var ipv4 = "";
			var ipv6 = "";
			var pmac = PhysicalAddress.None;

			foreach (var info in gatewayIPAddressInformation)
			{
				switch (info.Address.AddressFamily)
				{
					case AddressFamily.InterNetwork:
						ipv4 = info.Address.ToString();
						break;
					case AddressFamily.InterNetworkV6:
						ipv6 = info.Address.ToString();
						break;
				}
			}

			if (ipv4 != "")
				foreach (var entry in arpTable)
					if (entry.IPv4 == ipv4)
					{
						pmac = entry.MAC;
						break;
					}

			if (arpTable.Length <= 0)
			{
				var pingRes = new ArpPing().Send(ipv4.ToString(), true);
				if (pingRes.ArpStatus == ArpStatus.Success)
					pmac = pingRes.PhysicalAddress;
			}

			return pmac == null ? null : new Gateway(IPAddress.Parse(ipv4), pmac)
			{
				IPv6 = ipv6
			};
		}
	}
}
