﻿using System;
using System.Net;
using Newtonsoft.Json;

namespace NetStorm.Converters
{
	public class IPInfoJSONConverter : JsonConverter
	{
		public override bool CanConvert(Type objectType)
		{
			return objectType == typeof(IPNetwork);
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			throw new NotImplementedException();
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			if (value == null || !(value is IPNetwork netw))
			{
				serializer.Serialize(writer, null);
				return;
			}

			writer.WriteStartObject();

			writer.WritePropertyName("NetworkAddress");
			serializer.Serialize(writer, netw.Network.ToString(), typeof(string));

			writer.WritePropertyName("Netmask");
			serializer.Serialize(writer, netw.Netmask.ToString(), typeof(string));

			writer.WritePropertyName("CIDR");
			serializer.Serialize(writer, netw.Cidr.ToString(), typeof(string));

			writer.WritePropertyName("Broadcast");
			serializer.Serialize(writer, netw.Broadcast.ToString(), typeof(string));

			writer.WritePropertyName("Total");
			serializer.Serialize(writer, netw.Total.ToString(), typeof(string));

			writer.WriteEndObject();
		}
	}
}
