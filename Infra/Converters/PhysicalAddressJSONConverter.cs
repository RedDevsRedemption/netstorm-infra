﻿using Newtonsoft.Json;
using System;
using System.Net.NetworkInformation;

namespace NetStorm.Converters
{
	public class PhysicalAddressJSONConverter : JsonConverter
	{
		public override bool CanConvert(Type objectType)
		{
            return objectType == typeof(PhysicalAddress);
        }

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			throw new NotImplementedException();
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			if (value == null || !(value is PhysicalAddress addr))
			{
				serializer.Serialize(writer, null);
				return;
			}

			serializer.Serialize(writer, addr.ToCannonicalString(), typeof(string));
		}
	}
}
