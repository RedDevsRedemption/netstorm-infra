﻿using SharpPcap.LibPcap;
using SharpPcap.WinPcap;
using System;
using System.Globalization;
using System.Security.Principal;
using System.Threading;

namespace NetStorm
{
	public enum Subsystem
	{
		Windows,
		Unix,
		Mac
	}

	public enum NetworkAPI
	{
		Native,
		LibPcap,
		WinPcap
	}

	public enum PrivilegeLevel
	{
		Root,
		User
	}

	public class Config
	{
		public static Config Instance { get; } = new Config();

		public Subsystem Subsystem { get; private set; } = Subsystem.Unix;
		public PrivilegeLevel PrivilegeLevel { get; private set; } = PrivilegeLevel.User;
		public NetworkAPI NetworkAPI { get; private set; } = NetworkAPI.Native;
		public CultureInfo CurrentCulture { get; private set; }

		private Config()
		{
		}

		/// <summary>
		/// Analyze access levels
		/// </summary>
		public void Analyze()
		{
			Logger.Log.Normal("Analyzing environment");

			Subsystem = GetSubsystem();
			PrivilegeLevel = GetAccessLevel();
			NetworkAPI = GetAPIMode();
			CurrentCulture = Thread.CurrentThread.CurrentCulture;

			Logger.Log.Normal(ToString());
		}

		/// <summary>
		/// Get network api mode
		/// </summary>
		/// <returns></returns>
		private NetworkAPI GetAPIMode()
		{
			//Libpacp needs root access under Linux
			if (Subsystem == Subsystem.Unix && PrivilegeLevel == PrivilegeLevel.User)
				return NetworkAPI.Native;

			try
			{
				var winPcapInstance = WinPcapDeviceList.New();
				if (winPcapInstance != null)
				{
					if (winPcapInstance.Count == 0)
						return NetworkAPI.WinPcap;

					if (CheckInterface(winPcapInstance[0]))
						return NetworkAPI.WinPcap;
				}
			}
			catch { }
			try
			{
				var liPcapInstance = LibPcapLiveDeviceList.New();
				if (liPcapInstance != null)
				{
					if (liPcapInstance.Count == 0)
						return NetworkAPI.LibPcap;

					if (CheckInterface(liPcapInstance[0]))
						return NetworkAPI.LibPcap;
				}
			}
			catch { }
			return NetworkAPI.Native;
		}

		/// <summary>
		/// Check if interface api is working correctly
		/// </summary>
		/// <param name="device"></param>
		/// <returns></returns>
		private bool CheckInterface(PcapDevice device)
		{
			Logger.Log.Debug("Starting pcap check sequence...");
			try
			{
				device.Open();
				device.OnPacketArrival += Device_OnPacketArrival;
				Logger.Log.Debug("Starting pcap test capture...");
				device.StartCapture();
				device.StopCapture();
				Logger.Log.Debug("Test capture stopped");
				device.OnPacketArrival -= Device_OnPacketArrival;
				device.Close();
				Logger.Log.Debug("Pcap check sequence finished");
				return true;
			}
			catch (Exception e)
			{
				Logger.Log.Error($"Pcap check sequence failed: {e.Message}");
				return false;
			}
		}

		private void Device_OnPacketArrival(object sender, SharpPcap.CaptureEventArgs e) { }

		/// <summary>
		/// Get privilege level
		/// </summary>
		/// <returns></returns>
		private PrivilegeLevel GetAccessLevel()
		{
			try
			{
				return new WindowsPrincipal(WindowsIdentity.GetCurrent()).IsInRole(WindowsBuiltInRole.Administrator) ? PrivilegeLevel.Root : PrivilegeLevel.User;
			}
			catch { return PrivilegeLevel.User; }
		}

		/// <summary>
		/// Get subsystem (WIN or UNIX)
		/// </summary>
		/// <returns></returns>
		private Subsystem GetSubsystem()
		{
			int p = (int)Environment.OSVersion.Platform;
			return (p == 4) || (p == 6) || (p == 128) ? Subsystem.Unix : Subsystem.Windows;
		}

		public override string ToString()
		{
			return $"Subsystem: {Subsystem.ToString()};" +
				$" Access level: {PrivilegeLevel.ToString()};" +
				$" API level: {NetworkAPI.ToString()};" +
				$" Language: {CurrentCulture.CompareInfo.Name}";
		}
	}
}
