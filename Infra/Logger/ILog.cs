﻿namespace NetStorm.Logger
{
	public interface ILog
	{
		string ID { get; }
		void Log(LogEntry logEntry);
	}
}
