﻿using System;

namespace NetStorm.Logger
{
	public enum LogPriority
	{
		Low,
		Normal,
		Critical,
		Error,
		Debug
	}

	public class LogEntry
	{
		public string Message { get; private set; }
		public DateTime LogTime { get; private set; }
		public LogPriority Priority { get; private set; }
		public string Caller { get; private set; }

		public LogEntry(string caller, string message, LogPriority priority)
		{
			Message = message;
			Caller = caller;
			Priority = priority;
			LogTime = DateTime.Now;
		}

		public override string ToString()
		{
			return Priority.ToString().ToUpper()[0] + " " + LogTime.ToString() + " [" + Caller + "]: " + Message;
		}
	}
}
