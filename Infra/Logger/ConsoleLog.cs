﻿using System;

namespace NetStorm.Logger
{
	class ConsoleLog : ILog
	{
		public string ID => "Console output";

		public void Log(LogEntry logEntry)
		{
			Console.WriteLine(logEntry.ToString());
		}
	}
}
