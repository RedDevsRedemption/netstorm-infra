﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace NetStorm.Logger
{
    public class Log
	{
		private static readonly List<ILog> loggers;

		static Log()
		{
			loggers = new List<ILog>();
		}

		private Log() { }

		public static void Low(string msg, [CallerMemberName] string caller = "")
		{
			InternalLog(caller, msg, LogPriority.Low);
		}

		public static void Normal(string msg, [CallerMemberName] string caller = "")
		{
			InternalLog(caller, msg, LogPriority.Normal);
		}

		public static void Error(string msg, [CallerMemberName] string caller = "")
		{
			InternalLog(caller, msg, LogPriority.Error);
		}

		public static void Critical(string msg, [CallerMemberName] string caller = "")
		{
			InternalLog(caller, msg, LogPriority.Critical);
		}

		public static void Debug(string msg, [CallerMemberName] string caller = "")
		{
			InternalLog(caller, msg, LogPriority.Debug);
		}

		public static void Attach(ILog log)
		{
			lock (loggers)
				loggers.Add(log);

			InternalLog(log.ID, "Attached to log", LogPriority.Debug);
		}

		public static void Detach(ILog log)
		{
			try
			{
				lock (loggers)
					loggers.Remove(log);
				InternalLog(log.ID, "Detached from log", LogPriority.Debug);
			}
			catch { }
		}

		private static void InternalLog(string caller, string msg, LogPriority priority)
		{
			var v = new LogEntry(caller, msg, priority);
			foreach (var l in loggers)
				l.Log(v);
		}
	}
}
