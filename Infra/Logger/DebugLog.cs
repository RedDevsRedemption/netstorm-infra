﻿using System.Diagnostics;

namespace NetStorm.Logger
{
	public class DebugLog :ILog
	{
		public string ID => "Debug output";

		public void Log(LogEntry logEntry)
		{
			Debug.WriteLine(logEntry.ToString());
		}
	}
}
