﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;

namespace NetStorm.Classes
{
	public class Vendor
	{
		public static Vendor[] Vendors => oui.Values.ToArray();
		private static readonly Dictionary<string, Vendor> oui;
		public static Vendor None { get; } = new Vendor();
		public string Manufacturer { get; } = "";
		public string Address { get; } = "";
		public string Prefix { get; } = "";

		static Vendor()
		{
			oui = new Dictionary<string, Vendor>();
			LoadVendors();
		}

		private Vendor()
		{

		}
		
		public Vendor(string prefix, string manufacturer, string address)
		{
			Manufacturer = manufacturer;
			Address = address;
			Prefix = prefix;
		}

		private static void LoadVendors()
		{
			try
			{
				using (var reader = new StringReader(new AssemblyHelper().ReadAsText("NetStorm.Assets.Lists.oui.csv")))
				{
					var line = "";
					var splitted = new string[0];
					while (true)
					{
						line = reader.ReadLine();
						if (line == null)
							break;

						//line should look like this: MAC-prefix; Manufacturer; Address
						splitted = line.Split(new char[] { ';' });
						splitted[0] = splitted[0].ToUpper();
						try
						{
							oui.Add(splitted[0], new Vendor(splitted[0], Config.Instance.CurrentCulture.TextInfo.ToTitleCase(splitted[1].ToLower()), splitted[2]));
						}
						catch { }
					}
				}
				Logger.Log.Normal("Loading OUI complete");
			}
			catch (Exception e)
			{
				Logger.Log.Error("Failed to load OUI: " + e.Message);
			}
		}

		public static Vendor GetVendor(PhysicalAddress pAddr)
		{
			var addr = pAddr.ToVendorString().ToUpper();
			foreach (var v in oui)
				if (v.Key == addr)
					return v.Value;
			return None;
		}
	}
}
