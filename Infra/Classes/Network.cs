﻿using NetStorm.Classes.Devices;
using NetStorm.Classes.Lists;
using System.Linq;
using System.Net;

namespace NetStorm.Classes
{
	class Network
	{
		public string Name { get; private set; }
		public IPNetwork IPNetwork { get; private set; }
		public Gateway Gateway { get; private set; }
		public DeviceList Devices { get; }

		public Network(IPNetwork network)
		{
			IPNetwork = network;
			Name = "";

			Devices = new DeviceList();
			Devices.DeviceAdded += Devices_DeviceAdded;
		}

		public Network(IPNetwork network, Gateway gateway)
		{
			IPNetwork = network;
			Gateway = gateway;
			Name = "";

			Devices = new DeviceList();
		}

		/// <summary>
		/// Check if a new device is a gateway
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Devices_DeviceAdded(object sender, INetworkDevice e)
		{
			if (NetworkDevice.GetDeviceType(e) != DeviceType.Gateway)
				return;

			Gateway = new Gateway(e);
			Devices.DeviceAdded -= Devices_DeviceAdded;
		}

		public override string ToString()
		{
			return $"{IPNetwork.ToString()}, Gateway: {Gateway?.ToString()}, Devices: {Devices.Count()}";
		}
	}
}
