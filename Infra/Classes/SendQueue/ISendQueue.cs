﻿using NetStorm.Classes.Devices;
using PacketDotNet;
using System;
using System.Collections.Generic;

namespace NetStorm.Classes.SendQueue
{
	interface ISendQueue : IList<Packet>, IDisposable
	{
		void Transmit(INetworkInterface networkInterface);
		void AddRange(IEnumerable<Packet> collection);
	}
}
