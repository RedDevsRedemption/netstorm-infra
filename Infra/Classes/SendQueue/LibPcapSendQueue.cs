﻿using NetStorm.Classes.Devices;
using PacketDotNet;
using System;
using System.Collections.Generic;

namespace NetStorm.Classes.SendQueue
{
	class LibPcapSendQueue : List<Packet>, ISendQueue
	{
		public void Dispose()
		{
			Clear();
		}

		public void Transmit(INetworkInterface networkInterface)
		{
			if (!(networkInterface is IPcapNetworkInterface pcapNic))
				throw new Exception("Network interface must be a Pcap interface");

			try
			{
				foreach (var packet in this)
					pcapNic.PcapDevice.SendPacket(packet);
			}
			catch { }
		}
	}
}
