﻿using NetStorm.Classes.Devices;
using PacketDotNet;
using SharpPcap.WinPcap;
using System;
using System.Collections.Generic;

namespace NetStorm.Classes.SendQueue
{
	class WinPcapSendQueue : List<Packet>, ISendQueue
	{
		private SharpPcap.WinPcap.SendQueue sendQueue;

		public new Packet this[int index]
		{
			get => base[index];
			set
			{
				base[index] = value;
				InternalAdd();
			}
		}

		private void InternalAdd()
		{
			sendQueue?.Dispose();

			var lenght = 0;
			foreach (var packet in this)
				lenght += packet.Bytes.Length + 32;

			sendQueue = new SharpPcap.WinPcap.SendQueue(lenght);
			foreach (var packet in this)
				sendQueue.Add(packet.Bytes);
		}

		public void Transmit(INetworkInterface networkInterface)
		{
			if (!(networkInterface is IPcapNetworkInterface pcapNic))
				throw new Exception("Network interface must be a Pcap interface");

			if (!(pcapNic.PcapDevice is WinPcapDevice winpcapDev))
				throw new Exception("Network interface must be a WinPcap interface");

			if (winpcapDev != null && winpcapDev.Opened)
				sendQueue?.Transmit(winpcapDev, SharpPcap.WinPcap.SendQueueTransmitModes.Synchronized);
		}

		public new void Add(Packet item)
		{
			base.Add(item);
			InternalAdd();
		}

		public new void AddRange(IEnumerable<Packet> collection)
		{
			base.AddRange(collection);
			InternalAdd();
		}

		public new void Insert(int index, Packet item)
		{
			base.Insert(index, item);
			InternalAdd();
		}

		public new void RemoveAt(int index)
		{
			base.RemoveAt(index);
			InternalAdd();
		}

		public new void Clear()
		{
			base.Clear();
			InternalAdd();
		}

		public new bool Remove(Packet item)
		{
			if (base.Remove(item))
			{
				InternalAdd();
				return true;
			}
			return false;
		}

		public void Dispose()
		{
			try { sendQueue?.Dispose(); }
			catch { }
		}
	}
}
