﻿using NetStorm.Classes.Devices;
using PacketDotNet;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace NetStorm.Classes.SendQueue
{
	class SendQueue : ISendQueue
	{
		private readonly ISendQueue queue;

		public SendQueue()
		{
			queue = Config.Instance.NetworkAPI == NetworkAPI.Native ? new FallbackQueue() :
				Config.Instance.NetworkAPI == NetworkAPI.WinPcap ? (ISendQueue)new WinPcapSendQueue() : new LibPcapSendQueue();
		}

		public Packet this[int index] { get => this.queue[index]; set => this.queue[index] = value; }

		public int Count => this.queue.Count;

		public bool IsReadOnly => this.queue.IsReadOnly;

		public void Add(Packet item)
		{
			this.queue.Add(item);
		}

		public void AddRange(IEnumerable<Packet> collection)
		{
			this.queue.AddRange(collection);
		}

		public void Clear()
		{
			this.queue.Clear();
		}

		public bool Contains(Packet item)
		{
			return this.queue.Contains(item);
		}

		public void CopyTo(Packet[] array, int arrayIndex)
		{
			this.queue.CopyTo(array, arrayIndex);
		}

		public void Dispose()
		{
			this.queue.Dispose();
		}

		public IEnumerator<Packet> GetEnumerator()
		{
			return this.queue.GetEnumerator();
		}

		public int IndexOf(Packet item)
		{
			return this.queue.IndexOf(item);
		}

		public void Insert(int index, Packet item)
		{
			this.queue.Insert(index, item);
		}

		public bool Remove(Packet item)
		{
			return this.queue.Remove(item);
		}

		public void RemoveAt(int index)
		{
			this.queue.RemoveAt(index);
		}

		public void Transmit(INetworkInterface networkInterface)
		{
			this.queue.Transmit(networkInterface);
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		private class FallbackQueue : List<Packet>, ISendQueue
		{
			public void Dispose() { Clear(); }

			public void Transmit(INetworkInterface networkInterface) { }
		}
	}
}
