﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using NetStorm.Classes.Devices;
using NetStorm.Extensions;

namespace NetStorm.Classes.Lists
{
	class DeviceList : IEnumerable<INetworkDevice>
	{
		private readonly SortedDictionary<string, INetworkDevice> networkDevices;
		private readonly SortedDictionary<string, INetworkDevice> privateDevices;

		public static DeviceList Global { get; } = new DeviceList();
		public event EventHandler<INetworkDevice> DeviceAdded, DeviceRemoved;

		public DeviceList()
		{
			networkDevices = new SortedDictionary<string, INetworkDevice>(new VersionComparer());
			privateDevices = new SortedDictionary<string, INetworkDevice>(new VersionComparer());
		}

		public INetworkDevice this[IPAddress IPAddress]
		{
			get { return IPAddress.IsPrivate() ? privateDevices[IPAddress.ToString()] : networkDevices[IPAddress.ToString()]; }
		}

		public INetworkDevice this[string IPAddress]
		{
			get { return System.Net.IPAddress.TryParse(IPAddress, out var addr) ? this[addr] : null; }
		}

		public long Count => networkDevices.Count + privateDevices.Count;

		public void ClearPublic()
		{
			lock (networkDevices)
				networkDevices.Clear();
		}

		public void ClearPrivate()
		{
			lock (privateDevices)
				privateDevices.Clear();
		}

		public void Clear()
		{
			ClearPrivate();
			ClearPrivate();
		}

		public bool Contains(string ip)
		{
			lock (networkDevices)
				return networkDevices.ContainsKey(ip) || privateDevices.ContainsKey(ip);
		}

		public bool Add(INetworkDevice networkDevice, bool addGlobal = false)
		{
			var ip = networkDevice.IPv4 == "" ? networkDevice.IPv6 : networkDevice.IPv4;

			if (!IPAddress.TryParse(ip, out var addr))
				return false;

			if (addr.IsPrivate())
				lock (networkDevices)
					privateDevices.Add(addr.ToString(), networkDevice);
			else
				lock (privateDevices)
					networkDevices.Add(addr.ToString(), networkDevice);

			if (addGlobal && this != Global)
				Global.Add(networkDevice);

			DeviceAdded?.Invoke(this, networkDevice);

			return true;
		}

		public INetworkDevice Remove(string ip)
		{
			INetworkDevice dev = NetworkDevice.Empty;
			var rem = false;
			if (networkDevices.ContainsKey(ip))
			{
				dev = networkDevices[ip];
				rem = networkDevices.Remove(ip);
			}
			else if (privateDevices.ContainsKey(ip))
			{
				dev = privateDevices[ip];
				rem = privateDevices.Remove(ip);
			}

			if (rem)
				DeviceRemoved?.Invoke(this, dev);
			return dev;
		}

		public IEnumerator<INetworkDevice> GetEnumerator()
		{
			var devs = new List<INetworkDevice>();
			devs.AddRange(privateDevices.Values);
			devs.AddRange(networkDevices.Values);
			return devs.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			var devs = new List<INetworkDevice>();
			devs.AddRange(privateDevices.Values);
			devs.AddRange(networkDevices.Values);
			return devs.GetEnumerator();
		}
	}
}
