﻿using System;
using System.Collections.Generic;

namespace NetStorm.Classes.Lists
{
	public class RandomList<T> : List<T>
	{
		private readonly List<T> values;
		private Random random;

		public RandomList()
		{
			values = new List<T>();
			random = new Random();
		}

		public void Shuffle()
		{
			random = new Random(DateTime.Now.Year + DateTime.Now.Day + (int)DateTime.Now.DayOfWeek + DateTime.Now.DayOfYear + DateTime.Now.Minute + DateTime.Now.Millisecond + DateTime.Now.Hour);
			var excludedVals = new List<int>();
			var index = 0;

			base.Clear();
			for(int i = 0; i < values.Count; i++)
			{
				index = GetRandomValue(random, excludedVals);
				excludedVals.Add(index);
				base.Add(values[index]);
			}
		}

		private int GetRandomValue(Random random, List<int> excluded)
		{
			excluded.Sort();
			int res = random.Next(0, values.Count - excluded.Count - 1);
			for (int i = 0; i < excluded.Count; i++)
				if (res < excluded[i])
					return res;
				else
					res++;
			return res;
		}

		public new void AddRange(IEnumerable<T> items)
		{
			values.AddRange(items);
			Shuffle();
		}

		public new void Add(T item)
		{
			values.Add(item);
			Shuffle();
		}

		public new void Insert(int index, T item)
		{
			throw new NotSupportedException();
		}

		public new void RemoveAt(int index)
		{
			throw new NotSupportedException();
		}

		public new void Clear()
		{
			base.Clear();
			values.Clear();
		}
	}
}
