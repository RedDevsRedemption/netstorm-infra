﻿using NetStorm.Classes.Devices;
using System;
using System.ArrayExtensions;
using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;
using System.Timers;

namespace NetStorm.Classes.Utils
{
	class ArpCache
	{
		private const int DEFAULT_FLUSH = 2 * 60 * 1000;

		private static readonly ArpCache _instance = new ArpCache();
		private readonly Dictionary<string, PhysicalAddress> cache;
		private readonly Timer releaseTimer;

		private ArpCache()
		{
			releaseTimer = new Timer()
			{
				Interval = DEFAULT_FLUSH,
				AutoReset = true
			};
			cache = new Dictionary<string, PhysicalAddress>();
			releaseTimer.Start();
			releaseTimer.Elapsed += (_, __) => Recache();
			Recache();
		}

		private void Recache()
		{
			lock (cache)
			{
				cache.Clear();
				foreach (var c in ReadArpCache())
					cache.Add(c.Key, c.Value);
			}
		}

		public static PhysicalAddress GetEntry(string ipv4)
		{
			Dictionary<string, PhysicalAddress> cache = null;

			lock (_instance.cache)
				cache = _instance.cache;

			if (cache.ContainsKey(ipv4))
				return cache[ipv4];
			else
				_instance.Recache();

			if (cache.ContainsKey(ipv4))
				return cache[ipv4];
			return PhysicalAddress.None;
		}

		public static T GetEntry<T>(string ipv4) where T : INetworkDevice, new()
		{
			var res = new T
			{
				IPv4 = ipv4,
				MAC = GetEntry(ipv4)
			};
			return res;
		}

		public static INetworkDevice[] GetEntries(IPNetwork network)
		{
			Dictionary<string, PhysicalAddress> cache = null;

			lock (_instance.cache)
				cache = _instance.cache;

			var ret = new List<INetworkDevice>(cache.Count);
			foreach (var kp in cache)
				if (network.Contains(IPAddress.Parse(kp.Key)))
					ret.Add(new NetworkDevice(kp.Key)
					{
						MAC = kp.Value,
						Type = DeviceType.Internal
					});
			return ret.ToArray();
		}

		public static INetworkDevice[] GetEntries()
		{
			Dictionary<string, PhysicalAddress> cache = null;

			lock (_instance.cache)
				cache = _instance.cache;

			var res = new List<INetworkDevice>(cache.Count);
			foreach (var e in cache)
				res.Add(new NetworkDevice(e.Key)
				{
					MAC = e.Value,
					Type = DeviceType.Internal
				});
			return res.ToArray();
		}

		private static Dictionary<string, PhysicalAddress> ReadArpCache()
		{
			string output = Cmd.Execute("arp", "-a");
			string[] lines = output.Split('\n').RemoveEmptyEntries();

			string[] words;
			string ip = "";
			PhysicalAddress pmac = null;

			var cache = new Dictionary<string, PhysicalAddress>();

			foreach (var line in lines)
			{
				words = line.RemoveWhitespace(" ").Split(new char[] { ' ' });

				ip = "";
				pmac = PhysicalAddress.None;
				foreach (var word in words)
				{
					if (ip == "" && IPAddress.TryParse(word, out var parsedIP))
						ip = parsedIP.ToString();

					if (pmac == PhysicalAddress.None &&
						RegexCollection.PhysicalMacRegex.IsMatch(word) &&
						!RegexCollection.IPv4Regex.IsMatch(word))
						pmac = word.ToPhysicalAddress();

					if (pmac != PhysicalAddress.None && ip != "")
					{
						cache.TryAdd(ip, pmac);
						break;
					}
				}
			}

			return cache;
		}
	}
}
