﻿using NetStorm.Logger;
using System;
using System.Diagnostics;

namespace NetStorm.Classes.Utils
{
	public static class Cmd
	{
		public static void OpenUrl(string url)
		{
			try
			{
				Process.Start(url);
			}
			catch { }
		}

		public static string Execute(string filename, string args)
		{
			string output = "";
			using (Process p = new Process())
			{
				p.StartInfo = new ProcessStartInfo()
				{
					Arguments = args,
					CreateNoWindow = true,
					RedirectStandardOutput = true,
					UseShellExecute = false,
					FileName = filename,
				};
				try
				{
					p.Start();
					while (!p.StandardOutput.EndOfStream)
						output += p.StandardOutput.ReadLine() + Environment.NewLine;
				}
				catch (Exception e)
				{
					Log.Error("Process execute failed: " + e.Message);
				}
				p.Dispose();
			}
			return output;
		}
	}
}
