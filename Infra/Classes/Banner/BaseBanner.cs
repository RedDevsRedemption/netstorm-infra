﻿namespace NetStorm.Classes.Banner
{
	abstract class BaseBanner : IBanner
	{
		public string Banner { get; protected set; }

		public override string ToString()
		{
			return Banner;
		}
	}
}
