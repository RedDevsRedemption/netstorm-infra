﻿using NetStorm.Classes.NetworkTools;

namespace NetStorm.Classes.Banner
{
	class XPoweredBanner : BaseBanner
	{
		private const string fieldKey = "x-powered-by";

		public static XPoweredBanner None => new XPoweredBanner();

		public XPoweredBanner(HttpHeader header)
		{
			Banner = header.Fields[fieldKey];
		}

		private XPoweredBanner()
		{

		}

		public static bool IsImportant(HttpPacket packet, out XPoweredBanner authBanner)
		{
			authBanner = None;
			return packet.Header == null ? false : IsImportant(packet.Header, out authBanner);
		}

		public static bool IsImportant(HttpHeader header, out XPoweredBanner authBanner)
		{
			authBanner = None;
			if (!header.Fields.ContainsKey(fieldKey))
				return false;

			authBanner = new XPoweredBanner(header);
			return true;
		}
	}
}
