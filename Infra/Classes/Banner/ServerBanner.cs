﻿using NetStorm.Classes.NetworkTools;

namespace NetStorm.Classes.Banner
{
	class ServerBanner : BaseBanner
	{
		private const string fieldKey = "server";

		public static ServerBanner None => new ServerBanner();

		public ServerBanner(HttpHeader header)
		{
			Banner = header.Fields[fieldKey];
		}

		private ServerBanner()
		{

		}

		public static bool IsImportant(HttpPacket packet, out ServerBanner serverBanner)
		{
			serverBanner = None;
			return packet.Header == null ? false : IsImportant(packet.Header, out serverBanner);
		}

		public static bool IsImportant(HttpHeader header, out ServerBanner serverBanner)
		{
			serverBanner = None;
			if (!header.Fields.ContainsKey(fieldKey))
				return false;

			serverBanner = new ServerBanner(header);
			return true;
		}
	}
}
