﻿using System.Text.RegularExpressions;
using NetStorm.Classes.NetworkTools;
using NetStorm.Extensions;

namespace NetStorm.Classes.Banner
{
	class WWWAuthBanner : BaseBanner
	{
		private const string fieldKey = "www-authenticate";
		public new string Banner => Realm;
		public string Realm { get; private set; } = "";
		public string Type { get; private set; } = "";

		public static WWWAuthBanner None => new WWWAuthBanner();
		private readonly static Regex regex = new Regex("(.*?) realm=\"(.*?)\"", HttpHeader.HTTPRegexOptions);

		public WWWAuthBanner(HttpHeader header)
		{
			var content = header.Fields[fieldKey];
			var matches = regex.GetMatches(content);
			if(matches.Count >= 3)
			{
				Type = matches[1];
				Realm = matches[2];
			}
		}

		private WWWAuthBanner()
		{

		}

		public static bool IsImportant(HttpPacket packet, out WWWAuthBanner authBanner)
		{
			authBanner = None;
			return packet.Header == null ? false : IsImportant(packet.Header, out authBanner);
		}

		public static bool IsImportant(HttpHeader header, out WWWAuthBanner authBanner)
		{
			authBanner = None;
			if (!header.Fields.ContainsKey(fieldKey))
				return false;

			authBanner = new WWWAuthBanner(header);
			return true;
		}
	}
}
