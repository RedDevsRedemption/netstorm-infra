﻿using System.Text.RegularExpressions;
using NetStorm.Classes.NetworkTools;
using NetStorm.Extensions;

namespace NetStorm.Classes.Banner
{
	class TitleBanner : BaseBanner
	{
		public static TitleBanner None => new TitleBanner();
		private readonly static Regex regex = new Regex(@"<title>(.*?)<\/title>", HttpHeader.HTTPRegexOptions);

		public TitleBanner(HttpPacket packet)
		{
			var matches = regex.GetMatches(packet.Content);
			if (matches.Count == 2)
				Banner = matches[1].Trim();
		}

		private TitleBanner()
		{

		}

		public static bool IsImportant(HttpPacket packet, out TitleBanner titleBanner)
		{
			titleBanner = None;
			var isMatch = regex.IsMatch(packet.Content);
			if (isMatch)
				titleBanner = new TitleBanner(packet);
			return isMatch;
		}
	}
}
