﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace NetStorm.Classes
{
	public class VersionComparer : IComparer, IComparer<string>
	{
		public int Compare(object x, object y)
		{
			return Compare(x.ToString(), y.ToString());
		}

		public int Compare(string x, string y)
		{
			if (!Version.TryParse(x.ToString(), out var v1) || !Version.TryParse(y.ToString(), out var v2))
				return 0;

			return v1.CompareTo(v2);
		}
	}
}
