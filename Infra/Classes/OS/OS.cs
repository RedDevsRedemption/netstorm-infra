﻿using System;

namespace NetStorm.Classes.OS
{
	public class OS
	{
		public static OS None => new OS("", new Version());

		public string Name { get; }
		public Version Version { get; }

		public OS(string name)
		{
			this.Name = name;
		}

		public OS(string name, Version version)
		{
			this.Name = name;
			this.Version = version;
		}

		public static bool operator ==(OS left, OS right)
		{
			return left.Name == right.Name && left.Version.CompareTo(right) == 0;
		}

		public static bool operator !=(OS left, OS right)
		{
			return !(left == right);
		}

		public override bool Equals(object obj)
		{
			if (!(obj is OS os))
				return false;

			return this == os;
		}

		public override int GetHashCode()
		{
			return HashCode.Combine(this.Name, this.Version);
		}
	}
}
