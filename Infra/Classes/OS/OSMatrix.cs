﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace NetStorm.Classes.OS
{
	public class OSMatrix : IReadOnlyList<OSMatch>
	{
		public DateTime LastUpdated { get; }

		public int Count => matches.Count;

		public OSMatch this[int index]
		{
			get
			{
				int counter = 0;
				foreach(var kp in matches)
				{
					if (counter == index)
						return kp.Value;
					counter++;
				}
				return new OSMatch(OS.None);
			}
		}

		private readonly Dictionary<string, OSMatch> matches;

		public OSMatrix()
		{
			LastUpdated = DateTime.Now;
			matches = new Dictionary<string, OSMatch>();
			matches.Add("macos", new OSMatch(new OS("MacOS")));
			matches.Add("ios", new OSMatch(new OS("IOS")));

			matches.Add("winwork", new OSMatch(new OS("Windows Workgroups")));
			matches.Add("win95", new OSMatch(new OS("Windows 95")));
			matches.Add("winnt3", new OSMatch(new OS("Windows NT 3.XX")));
			matches.Add("winnt4", new OSMatch(new OS("Windows NT 4.XX")));
			matches.Add("win10", new OSMatch(new OS("Windows 10")));

			matches.Add("linux", new OSMatch(new OS("Linux")));
			matches.Add("android", new OSMatch(new OS("Android")));
			matches.Add("bsd", new OSMatch(new OS("BSD")));

			matches.Add("solaris", new OSMatch(new OS("Solaris")));
			matches.Add("aix", new OSMatch(new OS("AIX")));
			matches.Add("cisco", new OSMatch(new OS("Cisco")));
		}

		public OSMatch this[string os]
		{
			get
			{
				try { return matches[os]; }
				catch { return new OSMatch(OS.None); }
			}
		}

		public List<OS> BestMatches()
		{
			var highest = GetMaxMatch();

			if (highest == 0)
				return new List<OS>();

			var res = new List<OS>(matches.Count);

			foreach (var a in matches)
				if (a.Value.Match == highest)
					res.Add(a.Value.OS);

			return res;
		}

		public string BestMatchesToString()
		{
			var bestMatches = BestMatches();
			if (bestMatches.Count == 0)
				return "";

			var res = "";
			foreach (var o in bestMatches)
				res = res == "" ? o.Name : $"{o.Name} / {res}";

			return res;
		}

		private int GetMaxMatch()
		{
			var highest = 0;

			foreach (var a in matches)
				if (a.Value.Match > highest)
					highest = a.Value.Match;

			return highest;
		}

		public IEnumerator<OSMatch> GetEnumerator()
		{
			return matches.Values.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
	}

	public class OSMatch
	{
		public OS OS { get; } = OS.None;
		public int Match { get; set; } = 0;

		public OSMatch(OS os)
		{
			OS = os;
		}
	}
}
