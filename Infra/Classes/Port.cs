﻿using System;
using System.Collections.Generic;
using System.IO;
using NetStorm.Classes.Banner;

namespace NetStorm.Classes
{
	public enum Protocol
	{
		/// <summary>
		/// Transmission Control Protocol
		/// </summary>
		Tcp = System.Net.Sockets.ProtocolType.Tcp,
		/// <summary>
		/// User Datagram Protocol
		/// </summary>
		Udp = System.Net.Sockets.ProtocolType.Udp,
		/// <summary>
		/// Stream Control Transmission Protocol
		/// </summary>
		Sctp,
		/// <summary>
		/// Datagram Congestion Control Protocol
		/// </summary>
		Dccp,
		Unknown
	}

	public enum PortStatus
	{
		Open,
		Filtered,
		Closed,
		Unknown
	}

	public class Port
	{
		private static readonly List<Port> ports;
		public ushort Number { get; }
		public string Service { get; }
		public string Description { get; }
		public List<IBanner> Banners { get; } = new List<IBanner>();
		public Protocol Protocol { get; }
		public PortStatus PortStatus { get; set; }

		static Port()
		{
			ports = new List<Port>();
			LoadPorts();
		}

		public Port(ushort number, string portService, string description, Protocol type)
		{
			Number = number;
			Service = portService;
			Protocol = type;
			Description = description;
			PortStatus = PortStatus.Unknown;
		}

		public Port(ushort number, string portService, string description, Protocol type, PortStatus status)
		{
			Number = number;
			Service = portService;
			Protocol = type;
			Description = description;
			PortStatus = status;
		}

		public override string ToString()
		{
			var status = "";

			if (PortStatus == PortStatus.Closed)
				status = PortStatus.ToString();
			else if (PortStatus == PortStatus.Open)
				status = PortStatus.ToString();

			if (PortStatus == PortStatus.Filtered)
				status = status == "" ? PortStatus.ToString() : $"{status} | {PortStatus.ToString()}";

			if (PortStatus == PortStatus.Unknown)
				status = PortStatus.ToString();

			var res = $"{Protocol.ToString()} {Number} {Service} ({Description}) {status}";
			var banner = "";
			foreach (var b in Banners)
				banner = banner == "" ? b.Banner : banner + " | " + b.Banner;
			return Banners.Count == 0 ? res : res + $" - { banner }";
		}

		public override bool Equals(object obj)
		{
			var port = obj as Port;
			return port != null && this == port;
		}

		public static bool operator ==(Port left, Port right)
		{
			if (left == null || right == null)
				return false;

			return left.Protocol == right.Protocol && left.Number == right.Number;
		}

		public static bool operator !=(Port left, Port right)
		{
			return !(left == right);
		}

		private static Protocol GetProtocolType(string s)
		{
			s = s.ToLower();
			switch (s)
			{
				case "tcp":
					return Protocol.Tcp;
				case "udp":
					return Protocol.Udp;
				case "scpt":
					return Protocol.Sctp;
				case "dccp":
					return Protocol.Dccp;
				default:
					return Protocol.Unknown;
			}
		}

		private static void LoadPorts()
		{
			try
			{
				using (var reader = new StringReader(new AssemblyHelper().ReadAsText("NetStorm.Assets.Lists.port_services.csv")))
				{
					var line = "";
					var splitted = new string[0];
					while (true)
					{
						line = reader.ReadLine();
						if (line == null)
							break;

						splitted = line.Split(new char[] { ';' });
						var number = ushort.Parse(splitted[0]);
						var proto = GetProtocolType(splitted[1]);
						ports.Add(new Port(number, splitted[2], splitted[3], proto));
					}
				}
				Logger.Log.Normal("Loading port services complete");
			}
			catch (Exception e)
			{
				Logger.Log.Error("Failed to load port services: " + e.Message);
			}
		}

		public static Port GetPort(ushort number, Protocol protocolType)
		{
			foreach (var p in ports)
				if (p.Number == number && p.Protocol == protocolType)
					return p.Copy();
			return new Port(number, "", "", protocolType);
		}

		public static Port GetPort(ushort number, Protocol protocolType, PortStatus status)
		{
			foreach (var p in ports)
				if (p.Number == number && p.Protocol == protocolType)
				{
					var po = p.Copy();
					po.PortStatus = status;
					return po;
				}
			return new Port(number, "", "", protocolType);
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
	}
}
