﻿using System.Net;
using PacketDotNet;

namespace NetStorm.Classes.PacketFilter
{
	public class IPFilter : IPacketFilter
	{
		public IPAddress SrcAddress { get; set; } = IPAddress.Any;
		public IPAddress DstAddress { get; set; } = IPAddress.Any;

		public bool IsValid(EthernetPacket packet)
		{
			if (packet.PayloadPacket == null ||
				!(packet.PayloadPacket is IPPacket ip))
				return false;

			if (ip.DestinationAddress.ToString() != DstAddress.ToString() && DstAddress != IPAddress.Any)
				return false;

			if (ip.SourceAddress.ToString() != SrcAddress.ToString() && SrcAddress != IPAddress.Any)
				return false;

			return true;
		}
	}
}
