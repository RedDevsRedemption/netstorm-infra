﻿using PacketDotNet;

namespace NetStorm.Classes.PacketFilter
{
	public interface IPacketFilter
	{
		bool IsValid(EthernetPacket ethernetPacket);
	}
}
