﻿using PacketDotNet;

namespace NetStorm.Classes.PacketFilter
{
	public class TCPFilter : IPacketFilter
	{
		public readonly IPFilter IPFilter;

		public TCPFilter()
		{
			IPFilter = new IPFilter();
		}

		public TCPFilter(IPFilter filter)
		{
			IPFilter = filter;
		}

		public ushort SrcPort { get; set; } = 0;
		public ushort DstPort { get; set; } = 0;

		public bool IsValid(EthernetPacket packet)
		{
			if (!IPFilter.IsValid(packet) || 
				packet.PayloadPacket == null ||
				!(packet.PayloadPacket.PayloadPacket is TcpPacket tcp))
				return false;

			if (tcp.SourcePort != SrcPort && SrcPort != 0)
				return false;

			if (tcp.DestinationPort != DstPort && DstPort != 0)
				return false;

			return true;
		}
	}
}
