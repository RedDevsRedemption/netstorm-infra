﻿using PacketDotNet;

namespace NetStorm.Classes.PacketFilter
{
	public class ArpFilter : IPacketFilter
	{
		public bool IsValid(EthernetPacket packet)
		{
			return packet.PayloadPacket is ArpPacket;
		}
	}
}
