﻿using System;

namespace NetStorm.Classes.Location
{
	public class Location
	{
		public Coordinates Coordinates { get; set; } = new Coordinates();
		public string City { get; set; } = "";
		public string Country { get; set; } = "";
		public string CountryCode { get; set; } = "";
		public string Region { get; set; } = "";
		public string RegionCode { get; set; } = "";
		public string Timezone { get; set; } = "";
		public int ZIP { get; set; } = 0;

		public override bool Equals(object obj)
		{
			if (!(obj is Location l) || obj == null)
				return false;
			return this == l;
		}

		public override int GetHashCode()
		{
			return HashCode.Combine(this.Coordinates, this.Country, this.CountryCode, this.Region, this.RegionCode, this.Timezone, this.ZIP);
		}

		public static bool operator ==(Location left, Location right)
		{
			return left.Country == right.Country &&
				left.CountryCode == right.CountryCode &&
				left.Region == right.Region &&
				left.RegionCode == right.RegionCode &&
				left.Timezone == right.Timezone &&
				left.ZIP == right.ZIP &&
				left.Coordinates == right.Coordinates;
		}

		public static bool operator !=(Location left, Location right)
		{
			return !(left == right);
		}
	}
}
