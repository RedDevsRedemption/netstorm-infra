﻿using System;
using System.IO;

namespace NetStorm.Classes.Location
{
	public class IPInfo
	{
		public readonly static IPInfo None = new IPInfo();
		public Location Location { get; set; } = new Location();
		
		public string ISP { get; set; } = "";
		public string AS { get; set; } = "";
		public string Organisation { get; set; } = "";

		public static IPInfo ParseXML(string xml)
		{
			IPInfo result = null;
			using (TextReader sr = new StringReader(xml))
			{
				using (System.Data.DataSet dataBase = new System.Data.DataSet())
				{
					dataBase.ReadXml(sr);
					var parsingData = dataBase.Tables[0].Rows[0];

					if (parsingData[0].ToString().ToLower() != "success")
						return None;
					
					result = new IPInfo
					{
						Location = new Location
						{
							Country = parsingData[1].ToString(),
							CountryCode = parsingData[2].ToString(),
							RegionCode = parsingData[3].ToString(),
							Region = parsingData[4].ToString(),
							City = parsingData[5].ToString(),
							ZIP = int.TryParse(parsingData[6].ToString(), out var zip) ? zip : 0,
							Coordinates = new Coordinates
							{
								Latitude = decimal.TryParse(parsingData[7].ToString().Replace('.',','), out var lat) ? lat : 0,
								Longitude = decimal.TryParse(parsingData[8].ToString().Replace('.',','), out var lon) ? lon : 0
							},
							Timezone = parsingData[9].ToString(),
						},
						ISP = parsingData[10].ToString(),
						Organisation = parsingData[11].ToString(),
						AS = parsingData[12].ToString(),
					};
				}
			}
			return result;
		}

		public override string ToString()
		{
			return $"[IPInfo] ISP: { ISP } Country: {Location.Country } Region: { Location.Region }";
		}

		public static bool operator ==(IPInfo left, IPInfo right)
		{
			return left.ISP == right.ISP && left.AS == right.AS && left.Organisation == right.Organisation && left.Location == right.Location;
		}

		public static bool operator !=(IPInfo left, IPInfo right)
		{
			return !(left == right);
		}

		public override bool Equals(object obj)
		{
			if (!(obj is IPInfo i) || obj == null)
				return false;

			return this == i;
		}

		public override int GetHashCode()
		{
			return HashCode.Combine(this.Location, this.ISP, this.AS, this.Organisation);
		}
	}
}
