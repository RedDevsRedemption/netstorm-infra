﻿using System;

namespace NetStorm.Classes.Location
{
	public class Coordinates
	{
		public decimal Longitude { get; set; } = 0;
		public decimal Latitude { get; set; } = 0;

		public override string ToString()
		{
			return Longitude + "|" + Latitude;
		}

		public override bool Equals(object obj)
		{
			if (!(obj is Coordinates c) || obj == null)
				return false;

			return this == c;
		}

		public override int GetHashCode()
		{
			return HashCode.Combine(this.Longitude, this.Latitude);
		}

		public static bool operator ==(Coordinates left, Coordinates right)
		{
			return left.Latitude == right.Latitude &&
				left.Longitude == right.Longitude;
		}

		public static bool operator !=(Coordinates left, Coordinates right)
		{
			return !(left == right);
		}
	}
}
