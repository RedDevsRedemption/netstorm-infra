﻿using System;
using System.IO;
using System.Reflection;

namespace NetStorm.Classes
{
	public class AssemblyHelper
	{
		public string ReadAsText(string assemblyPath)
		{
			string file = "";
			var assembly = typeof(AssemblyHelper).GetTypeInfo().Assembly;

			using (var stream = assembly.GetManifestResourceStream(assemblyPath))
			{
				using (var reader = new StreamReader(stream))
				{
					file = reader.ReadToEnd();
				}
			}
			Logger.Log.Debug($"Reading file: {assemblyPath}");
			return file;
		}
	}
}
