﻿using NetStorm.Extensions;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace NetStorm.Classes.NetworkTools
{
	public enum PacketType
	{
		Request,
		Response
	}

	public enum RequestType
	{
		POST,
		GET,
		None
	}

	public class HttpPacket
	{
		public const string HeaderSeperator = "\r\n\r\n";
		public HttpHeader Header { get; private set; }
		public List<string[]> PostParams { get; private set; } = new List<string[]>();
		public string Content { get; private set; } = "";

		public HttpPacket(string packet)
		{
			var parts = packet.Split(HeaderSeperator);
			if (packet.Length >= 2)
			{
				Header = new HttpHeader(parts[0].Trim());
				Content = parts[1].Trim();
			}
			else
			{
				Header = HttpHeader.None;
				Content = "";
			}
		}

		public override string ToString()
		{
			return Header.ToString() + HeaderSeperator + Content;
		}
	}

	public class HttpHeader
	{
		public PacketType PacketType { get; private set; }
		public RequestType RequestType { get; private set; }
		public int StatusCode { get; private set; } = -1;
		public string StatusText { get; private set; }
		public Version HTTPVersion { get; private set; }
		public string Path { get; private set; } = "";
		public string Host { get; private set; } = "";
		public Dictionary<string, string> Fields { get; private set; } = new Dictionary<string, string>();
		public List<string[]> Params { get; private set; } = new List<string[]>();

		public static HttpHeader None => new HttpHeader("");

		public static readonly RegexOptions HTTPRegexOptions = RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase;

		private static readonly Regex responseRegex = new Regex(@"HTTP\/(.?).(.?) (.*?) (.*)", HTTPRegexOptions);
		private static readonly Regex pathRegex = new Regex(@"(GET|POST) (.*?) HTTP\/1\.(0|1)", HTTPRegexOptions);

		public HttpHeader(string header)
		{
			if (header == string.Empty) return;

			PacketType = GetPacketType(header);
			RequestType = GetRequestType(header);

			// HTTP/[Major].[Minor] [StatusCode] [StatusText]
			var matches = responseRegex.GetMatches(header);
			if (matches != null && matches.Count == 5)
			{
				HTTPVersion = new Version(int.Parse(matches[1]), int.Parse(matches[2]));
				StatusCode = int.Parse(matches[3]);
				StatusText = matches[4].Split('\n')[0].Trim();
			}

			switch (PacketType)
			{
				case PacketType.Request:
					Path = GetPath(header);
					Params.AddRange(GetParams(header));

					break;
			}

			Fields.AddRange(GetHeaderFields(header));
		}

		public HttpHeader(RequestType requestType, string host, Version version)
		{
			Init(requestType, host, "/", version);
		}

		public HttpHeader(RequestType requestType, string host, string path, Version version)
		{
			Init(requestType, host, path, version);
		}

		private void Init(RequestType requestType, string host, string path, Version version)
		{
			StatusCode = 0;
			PacketType = PacketType.Request;
			RequestType = requestType;
			Path = path;
			HTTPVersion = version;
			Host = host;
			Fields.Add("Host", host);
		}

		private string GetPath(string header)
		{
			var pathMatches = pathRegex.GetMatches(header);
			if (pathMatches == null || pathMatches.Count <= 1)
				return "/";

			return pathMatches[2];
		}

		private RequestType GetRequestType(string header)
		{
			if (header.Length <= 4)
				return RequestType.None;

			return header.Substring(0, 3) == "GET" ?
				RequestType.GET :
				RequestType.POST;
		}

		private PacketType GetPacketType(string header)
		{
			return header.Length > 4 && (header.Substring(0, 3) == "GET" || header.Substring(0, 4) == "POST") ?
				PacketType.Request :
				PacketType.Response;
		}

		private IDictionary<string, string> GetHeaderFields(string header)
		{
			var fields = header.Split('\n');

			var head = new string[0];
			var list = new Dictionary<string, string>(fields.Length);
			for (int i = 1; i < fields.Length; i++)
			{
				head = fields[i].Split(':');
				list.Add(head[0].ToLower(), head[1].Trim());
			}

			return list;
		}

		private IEnumerable<string[]> GetParams(string header)
		{
			var getLine = Path.Split('?');

			if (getLine.Length != 2)
				return new string[0][];

			var res = new List<string[]>();
			var getParams = getLine[1].Split('&');

			foreach (var param in getParams)
			{
				var splitParam = param.Split('=');
				if (splitParam.Length == 2) res.Add(new string[] { splitParam[0] ?? string.Empty, splitParam[1] ?? string.Empty });
			}

			return res;
		}

		public override string ToString()
		{
			string res = "";

			if (PacketType == PacketType.Response)
				res = "HTTP/" + HTTPVersion.ToString() + " " + StatusCode.ToString() + " " + StatusText;
			else
				res = RequestType.ToString().ToUpper() + " " + Path + " HTTP/" + HTTPVersion?.ToString();

			foreach (var kp in Fields)
				res = res + "\n" + kp.Key + ": " + kp.Value;

			return res;
		}
	}
}
