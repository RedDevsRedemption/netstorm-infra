﻿using NetStorm.Classes.Devices;
using NetStorm.Classes.Utils;
using NetStorm.Logger;
using NetStorm.Modules;
using PacketDotNet;
using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace NetStorm.Classes.NetworkTools
{
	public interface IArpPing
	{
		ArpReply Send(string ipv4);
		Task<ArpReply> SendAsync(string ipv4);
	}

	public enum ArpStatus
	{
		Success,
		DestinationUnreachable,
		Failed
	}

	public class ArpReply
	{
		public PhysicalAddress PhysicalAddress { get; }

		public IPAddress IPAddress { get; }
		public ArpStatus ArpStatus { get; }
		public DateTime Sent { get; }

		public DateTime Received => Sent.Add(RoundtipTime);
		public TimeSpan RoundtipTime { get; }

		private ArpReply()
		{
			IPAddress = IPAddress.None;
			ArpStatus = ArpStatus.Failed;
			Sent = DateTime.Now;
			RoundtipTime = new TimeSpan(0);
		}

		public ArpReply(IPAddress ipAddress, PhysicalAddress physicalAddress, DateTime pingSent, DateTime pingReceived, ArpStatus status)
		{
			this.IPAddress = ipAddress;
			this.PhysicalAddress = physicalAddress;
			this.Sent = pingSent;
			this.ArpStatus = status;
			RoundtipTime = pingReceived - pingSent;
		}

		public ArpReply(IPAddress ipAddress, PhysicalAddress physicalAddress, DateTime pingSent, TimeSpan responseTime, ArpStatus status)
		{
			this.IPAddress = ipAddress;
			this.PhysicalAddress = physicalAddress;
			this.Sent = pingSent;
			this.ArpStatus = status;
			this.RoundtipTime = responseTime;
		}

		public override string ToString()
		{
			return "[ArpResult] Status: " + ArpStatus + ", IPv4: " + IPAddress.ToString() + ", MAC: " + PhysicalAddress.ToCannonicalString() + ", Milliseconds: " + RoundtipTime.Milliseconds;
		}
	}

	public class ArpPing : IArpPing
	{
		private const int unreachable = 0x43;
		private const int success = 0x00;
		public static bool ArpAvailable
		{
			get
			{
				if (Config.Instance.NetworkAPI != NetworkAPI.Native && Core.Instance.CurrentInterface.IsCaputring)
					return true;
				if (Config.Instance.Subsystem == Subsystem.Windows)
					return true;
				return false;
			}
		}

		private ArpReply InternalSend(string ipv4)
		{
			var res = new ArpReply(IPAddress.None, PhysicalAddress.None, DateTime.Now, DateTime.Now, ArpStatus.Failed);

			if (!IPAddress.TryParse(ipv4, out var ip))
				return res;
			if (ip.AddressFamily != AddressFamily.InterNetwork)
				return res;
			if (Config.Instance.Subsystem != Subsystem.Windows)
				return new ArpReply(ip, PhysicalAddress.None, DateTime.Now, DateTime.Now, ArpStatus.Failed); ;

			var sent = DateTime.Now;
			var ipAddr = BitConverter.ToUInt32(ip.GetAddressBytes(), 0);

			try
			{
				byte[] macByte = new byte[6];
				int length = macByte.Length;
				int errorCode = NativeMethods.SendARP(ipAddr, 0, macByte, ref length);
				switch (errorCode)
				{
					case success:
						string mac = BitConverter.ToString(macByte, 0, length);
						res = new ArpReply(ip, PhysicalAddress.Parse(mac), sent, DateTime.Now, ArpStatus.Success);
						break;

					case unreachable:
						res = new ArpReply(ip, PhysicalAddress.None, sent, DateTime.Now, ArpStatus.DestinationUnreachable);
						break;

					default:
						res = new ArpReply(ip, PhysicalAddress.None, sent, DateTime.Now, ArpStatus.Failed);
						break;
				}
			}
			catch (Exception e)
			{
				res = new ArpReply(IPAddress.None, PhysicalAddress.None, DateTime.Now, DateTime.Now, ArpStatus.Failed);
				Log.Error("SendArp failed: " + e.Message);
			}
			return res;
		}

		public ArpReply Send(string ipv4)
		{
			return Send(ipv4, false);
		}

		public ArpReply Send(string ipv4, bool pingOnFailed)
		{
			var res = InternalSend(ipv4);

			if (pingOnFailed && res.ArpStatus != ArpStatus.Success)
			{
				var ping = new FallbackArpPing();
				res = ping.Send(ipv4);
			}

			return res;
		}

		public async Task<ArpReply> SendAsync(string ipv4, bool pingOnFailed)
		{
			return await Task.Run(() => Send(ipv4, pingOnFailed));
		}

		public Task<ArpReply> SendAsync(string ipv4)
		{
			return SendAsync(ipv4, false);
		}

		public static IArpPing GetEnvironmentArpPing()
		{
			if (Config.Instance.NetworkAPI != NetworkAPI.Native && Core.Instance.CurrentInterface != null && Core.Instance.CurrentInterface.IsCaputring)
				return new PcapArpPing();
			if (Config.Instance.Subsystem == Subsystem.Windows)
				return new ArpPing();
			return new FallbackArpPing();
		}
	}

	public class PcapArpPing : IArpPing
	{
		private const int defaultTimeout = 5000;
		private const int defaultSleep = 10;
		private ArpReply result;
		private readonly object mutex = new object();

		private void OnPacketArrived(Packet e, IPAddress target, DateTime startTime)
		{
			if (e is EthernetPacket eth)
				if (eth.PayloadPacket is ArpPacket arp)
					if (arp.SenderProtocolAddress.ToString() == target.ToString())
						result = new ArpReply(target, arp.SenderHardwareAddress, startTime, DateTime.Now, ArpStatus.Success);
		}

		private EthernetPacket GenerateRequest(IPAddress target)
		{
			var nic = Core.Instance.CurrentInterface;
			var eth = new EthernetPacket(nic.MAC, PhysicalAddressExtension.Broadcast, EthernetType.Arp);
			var arp = new ArpPacket(ArpOperation.Request, PhysicalAddressExtension.Broadcast, target, nic.MAC, IPAddress.Parse(nic.IPv4));
			eth.PayloadPacket = arp;
			eth.UpdateCalculatedValues();
			return eth;
		}

		public ArpReply Send(string ipv4)
		{
			return Send(ipv4, defaultTimeout);
		}

		public ArpReply Send(string ipv4, int timeout_MS)
		{
			lock (mutex)
			{
				var startTime = DateTime.Now;
				var target = IPAddress.Parse(ipv4);
				result = new ArpReply(target, PhysicalAddress.None, startTime, DateTime.Now, ArpStatus.Failed);
				try
				{
					var nic = Core.Instance.CurrentInterface;
					var handler = new EventHandler<Packet>((_, __) => OnPacketArrived(__, target, startTime));
					nic.PacketCaptured += handler;
					nic.Transmit(GenerateRequest(target));
					int slept = 0;
					while (timeout_MS >= slept && result.ArpStatus != ArpStatus.Success)
					{
						Thread.Sleep(defaultSleep);
						slept += defaultSleep;
					}
					nic.PacketCaptured -= handler;

					if (result.ArpStatus != ArpStatus.Success)
						result = new ArpReply(target, PhysicalAddress.None, startTime, DateTime.Now, ArpStatus.DestinationUnreachable);
				}
				catch { }
				return result;
			}
		}

		public Task<ArpReply> SendAsync(string ipv4)
		{
			return Task.Run(() => Send(ipv4, defaultTimeout));
		}

		public Task<ArpReply> SendAsync(string ipv4, int timeout_MS)
		{
			return Task.Run(() => Send(ipv4, timeout_MS));
		}
	}

	public class FallbackArpPing : IArpPing
	{
		public ArpReply Send(string ipv4)
		{
			if (!IPAddress.TryParse(ipv4, out var ip))
				return SendFailed(IPAddress.None);

			ArpReply reply = SendFailed(ip);

			if (ip.AddressFamily != AddressFamily.InterNetwork)
				return reply;

			try
			{
				using (var p = new Ping())
				{
					var startTime = DateTime.Now;
					var res = p.Send(ip.ToString());
					if (res.Status != IPStatus.Success)
						return SendFailed(ip);

					var device = ArpCache.GetEntry<NetworkDevice>(ipv4);
					var status = device.MAC == PhysicalAddress.None ? ArpStatus.Failed : ArpStatus.Success;
					reply = new ArpReply(ip, device.MAC, startTime, new TimeSpan(res.RoundtripTime * TimeSpan.TicksPerMillisecond), status);
				}
			}
			catch (PingException) { }
			catch (Exception e) { Log.Error($"Ping to {ipv4} failed: { e.Message }"); }

			return reply;
		}

		public Task<ArpReply> SendAsync(string ipv4)
		{
			return Task.Run(() => Send(ipv4));
		}

		private ArpReply SendFailed(IPAddress address)
		{
			return new ArpReply(address, PhysicalAddress.None, DateTime.Now, DateTime.Now, ArpStatus.Failed);
		}
	}
}