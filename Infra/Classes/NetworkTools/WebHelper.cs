﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace NetStorm.Classes.NetworkTools
{
	class WebHelper
	{
		public HttpPacket Get(string url)
		{
			var res = new HttpPacket("");

			try
			{
				using (var wclient = new WebClient())
				{
					wclient.Encoding = Encoding.UTF8;
					var content = wclient.DownloadString(url);
					var header = "HTTP/1.1 0 Unknown";
					for (int i = 0; i < wclient.ResponseHeaders.Count; i++)
						header = header + " \n" + wclient.ResponseHeaders.Keys[i] + ": " + wclient.ResponseHeaders.GetValues(i)[0];
					res = new HttpPacket(header + HttpPacket.HeaderSeperator + content);
				}
			}
			catch (Exception e) { Logger.Log.Debug(e.Message); }

			return res;
		}

		public HttpPacket Get(string host, ushort port)
		{
			return Get(host, "/", IPAddress.Any, port);
		}

		public HttpPacket Get(string host, string path, ushort port)
		{
			return Get(host, path, IPAddress.Any, port);
		}

		public HttpPacket Get(string host, string path, IPAddress bindingAddress)
		{
			return Get(host, path, bindingAddress, 80);
		}

		public HttpPacket Get(string host, string path, IPAddress bindingAddress, ushort destination)
		{
			return Get(host, path, bindingAddress, 0, destination);
		}

		public HttpPacket Get(string host, string path, IPAddress bindingAddress, ushort source, ushort destination)
		{
			var header = new HttpHeader(RequestType.GET, host, path, new Version(1, 1));
			header.Fields.Add("Connection", "close");
			header.Fields.Add("Accept-Encoding", "gzip, deflate");
			header.Fields.Add("User-Agent", "Mozilla/5.0 (X11; Linux x86_64; rv:12.0) Gecko/20100101 Firefox/12.0");
			return Get(header, bindingAddress, source, destination);
		}

		public HttpPacket Get(HttpHeader header, IPAddress bindingAddress, ushort src, ushort dst)
		{
			HttpPacket res = new HttpPacket("");
			try
			{
				using (var client = new TcpClient(new IPEndPoint(bindingAddress, src)))
				{
					Logger.Log.Debug($"Binding GET to {bindingAddress.ToString()}:{((IPEndPoint)client.Client.LocalEndPoint).Port}");
					client.ReceiveTimeout = 5000;
					client.Connect(header.Host, dst);

					if (!client.Connected)
						throw new Exception("Unable to connect to host: " + header.Host);

					using (NetworkStream ns = client.GetStream())
					{
						using (System.IO.StreamWriter sw = new System.IO.StreamWriter(ns))
						{
							using (System.IO.StreamReader sr = new System.IO.StreamReader(ns))
							{
								sw.Write(header.ToString() + HttpPacket.HeaderSeperator);
								sw.Flush();
								var a = sr.ReadToEnd();
								res = new HttpPacket(a);
								sr.Close();
							}
							sw.Close();
						}
						ns.Close();
					}
					client.Close();
				}
			}
			catch (Exception e) { Logger.Log.Debug(e.Message); }

			return res;
		}
	}
}
