﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace NetStorm.Classes.BackgroundWorker
{
	abstract class AbstractBackgroundWorker<T> : IBackgroundWorker
	{
		public bool Running
		{
			get => running;
			protected set
			{
				if (running == value)
					return;

				running = value;
				OnPropertyChanged();
			}
		}
		public string Name { get; }

		protected ResetToken token;
		private bool running;

		public event PropertyChangedEventHandler PropertyChanged;
		public abstract event EventHandler Exit;
		public abstract event EventHandler<T> DoWork;

		protected void OnPropertyChanged([CallerMemberName] string caller = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(caller));
		}

		public AbstractBackgroundWorker(string name)
		{
			Name = name;
			running = false;
			token = new ResetToken();
		}

		public virtual void Abort()
		{
			token.Reset();
		}

		public abstract bool Start();
		public abstract void Stop();
		public abstract void WaitForExit();
	}
}
