﻿using System;
using System.Threading;

namespace NetStorm.Classes.BackgroundWorker
{
	public class ResetToken
	{
		private const int step = 50;
		private bool canContinue;

		public bool CanContinue()
		{
			return canContinue;
		}

		public bool CanContinue(int millis)
		{
			Sleep(millis);
			return canContinue;
		}

		public bool CanContinue(TimeSpan timeSpan)
		{
			Sleep(timeSpan.Milliseconds);
			return canContinue;
		}

		private void Sleep(int millis)
		{
			int rest = millis % step;

			for (int i = 0; i < millis; i += step)
			{
				if (canContinue)
					Thread.Sleep(step);
				else
					break;
			}

			if (canContinue)
				Thread.Sleep(rest);
		}

		public void Set()
		{
			canContinue = true;
		}

		public void Reset()
		{
			canContinue = false;
		}
	}
}
