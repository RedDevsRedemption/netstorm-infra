﻿using System;
using System.Threading.Tasks;
using System.Timers;

namespace NetStorm.Classes.BackgroundWorker
{
	class TimerWorker : AbstractBackgroundWorker<EventArgs>
	{
		private Timer timer;

		public override event EventHandler Exit;
		public override event EventHandler<EventArgs> DoWork;

		public int IntervalMS { get; set; } = 100;
		private readonly ResetToken taskFinished;

		public TimerWorker(string name) : base(name)
		{
			taskFinished = new ResetToken();
			taskFinished.Reset();
		}

		public TimerWorker(string name, int intervalMS) : base(name)
		{
			IntervalMS = intervalMS;
			taskFinished = new ResetToken();
			taskFinished.Reset();
		}

		~TimerWorker()
		{
			timer?.Dispose();
		}

		public override bool Start()
		{
			InitTimer();
			Running = true;
			Task.Run(() => Timer_Elapsed(null, null));
			return true;
		}

		private void InitTimer()
		{
			timer?.Dispose();
			timer = new Timer()
			{
				Interval = IntervalMS,
				Enabled = true,
				AutoReset = true
			};
			timer.Elapsed += Timer_Elapsed;
			token.Set();
		}

		private void Timer_Elapsed(object sender, ElapsedEventArgs e)
		{
			if (!token.CanContinue())
				OnExit();
			else
				OnCycle();
		}

		private void OnExit()
		{
			Stop();
			Exit?.Invoke(this, new EventArgs());
		}

		private void OnCycle()
		{
			taskFinished.Set();
			timer.Stop();
			DoWork?.Invoke(this, new EventArgs());
			taskFinished.Reset();

			if (token.CanContinue())
			{
				InitTimer();
				timer.Start();
			}
			else
				OnExit();
		}

		public override void Stop()
		{
			Abort();
			timer?.Stop();
			Running = false;
		}

		public override void WaitForExit()
		{
			while (taskFinished.CanContinue(250) || token.CanContinue(250)) ;
		}
	}
}
