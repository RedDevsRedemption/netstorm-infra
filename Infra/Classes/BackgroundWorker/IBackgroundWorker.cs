﻿using System;
using System.ComponentModel;

namespace NetStorm.Classes.BackgroundWorker
{
	public interface IBackgroundWorker : INotifyPropertyChanged
	{
		event EventHandler Exit;
		bool Running { get; }
		string Name { get; }

		/// <summary>
		/// Start background worker operation
		/// </summary>
		/// <returns></returns>
		bool Start();

		/// <summary>
		/// Abort and wait until the background worker stopped
		/// </summary>
		void Stop();

		/// <summary>
		/// Abort background worker
		/// </summary>
		void Abort();

		/// <summary>
		/// Wait until the background worker stopped
		/// </summary>
		void WaitForExit();
	}
}
