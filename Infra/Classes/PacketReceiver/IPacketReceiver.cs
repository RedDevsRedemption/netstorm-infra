﻿using NetStorm.Classes.PacketFilter;

namespace NetStorm.Classes.PacketReceiver
{
	interface IPacketReceiver
	{
		void AddPacketFilter(IPacketFilter packetFilter);
		void RemovePacketFilter(IPacketFilter packetFilter);
	}
}
