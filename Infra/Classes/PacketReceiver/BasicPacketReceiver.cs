﻿using System;
using System.Collections.Generic;
using System.Threading;
using NetStorm.Classes.BackgroundWorker;
using NetStorm.Classes.Devices;
using NetStorm.Classes.PacketFilter;
using PacketDotNet;

namespace NetStorm.Classes.PacketReceiver
{
	public enum HistoryMode
	{
		None,
		Keep
	}

	class BasicPacketReceiver : AbstractBackgroundWorker<IEnumerable<Packet>>, IPacketReceiver
	{
		protected HistoryMode HistoryMode { get; set; }

		public override event EventHandler Exit;
		public override event EventHandler<IEnumerable<Packet>> DoWork;

		private List<Packet> packetQueue;
		private Thread bgWorker;
		private HashSet<IPacketFilter> packetFilters;
		private INetworkInterface nic;

		public BasicPacketReceiver() : base("Packet Receiver")
		{
			Init(HistoryMode.None);
		}

		public BasicPacketReceiver(HistoryMode historyMode) : base("Packet Receiver")
		{
			Init(historyMode);
		}

		private void Init(HistoryMode mode)
		{
			HistoryMode = mode;
			packetQueue = new List<Packet>();
			packetFilters = new HashSet<IPacketFilter>();
		}

		private void OnPacketCaptured(object sender, Packet e)
		{
			if (Running)
				lock (packetQueue)
					packetQueue.Add(e);
		}

		private void DoCycle()
		{
			Running = true;
			List<Packet> packets = new List<Packet>();
			do
			{
				if (HistoryMode == HistoryMode.None)
					packets.Clear();

				lock (packetQueue)
				{
					if (packetFilters.Count == 0)
						packets.AddRange(packetQueue);
					else
						foreach (var p in packetQueue)
							if (p is EthernetPacket e)
								foreach (var f in packetFilters)
									if (f.IsValid(e))
										packets.Add(p);
					packetQueue.Clear();
				}
				DoWork?.Invoke(this, packets);
			} while (token.CanContinue(100));
			GC.Collect();
			Exit?.Invoke(this, new EventArgs());
			Running = false;
		}

		public bool Start(INetworkInterface nic)
		{
			if (Running ||
				nic == null ||
				!nic.IsCaputring)
				return false;

			this.nic = nic;
			nic.PacketCaptured += OnPacketCaptured;
			token.Set();
			bgWorker = new Thread(DoCycle)
			{
				Name = Name
			};
			bgWorker.Start();
			return true;
		}

		public override bool Start()
		{
			return Start(Core.Instance.CurrentInterface);
		}

		public void AddPacketFilter(IPacketFilter packetFilter)
		{
			lock (packetFilters)
				if (!packetFilters.Contains(packetFilter))
					packetFilters.Add(packetFilter);
		}

		public void RemovePacketFilter(IPacketFilter packetFilter)
		{
			lock (packetFilters)
				packetFilters.Remove(packetFilter);
		}

		public override void Stop()
		{
			Abort();
			WaitForExit();
		}

		public override void Abort()
		{
			if (nic != null)
				nic.PacketCaptured -= OnPacketCaptured;

			token.Reset();
		}

		public override void WaitForExit()
		{
			bgWorker?.Join();
		}
	}
}
