﻿using NetStorm.Classes.Location;
using NetStorm.Classes.OS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Net.NetworkInformation;
using System.Runtime.CompilerServices;

namespace NetStorm.Classes.Devices
{
	class NetworkDevice : INetworkDevice
	{
		public static readonly NetworkDevice Empty = new NetworkDevice();

		public string IPv4
		{
			get => ipv4;
			set
			{
				if (string.IsNullOrEmpty(value))
					return;

				if (ipv4.ToString() == value.ToString())
					return;

				if (!IPAddress.TryParse(value, out var ip))
					throw new Exception("Received malformed ip address");
				if (ip.AddressFamily != System.Net.Sockets.AddressFamily.InterNetwork)
					throw new Exception("IPAddress family expected " + System.Net.Sockets.AddressFamily.InterNetwork.ToString() + " but got " + ip.AddressFamily);

				ipv4 = value;
				OnPropertyChanged();
			}
		}

		public string IPv6
		{
			get => ipv6;
			set
			{
				if (string.IsNullOrEmpty(value))
					return;

				if (ipv6.ToString() == value.ToString())
					return;

				if (!IPAddress.TryParse(value, out var ip))
					throw new Exception("Received malformed ip address");
				if (ip.AddressFamily != System.Net.Sockets.AddressFamily.InterNetworkV6)
					throw new Exception("IPAddress family expected " + System.Net.Sockets.AddressFamily.InterNetworkV6.ToString() + " but got " + ip.AddressFamily);

				ipv6 = value;
				OnPropertyChanged();
			}
		}

		public PhysicalAddress MAC
		{
			get => mac;
			set
			{
				if (mac.ToString() == value.ToString())
					return;
				mac = value;
				OnPropertyChanged();
				MACVendor = Vendor.GetVendor(mac);
			}
		}

		public Vendor MACVendor
		{
			get => macVendor;
			protected set
			{
				if (macVendor == value)
					return;
				macVendor = value;
				OnPropertyChanged();
			}
		}

		public string Hostname
		{
			get => hostname;
			set
			{
				if (hostname == value)
					return;
				hostname = value;
				OnPropertyChanged();
			}
		}

		public DeviceType Type
		{
			get => type;
			set
			{
				if (type == value)
					return;
				type = value;
				OnPropertyChanged();
			}
		}
		public DeviceState DeviceState
		{
			get => state;
			set
			{
				if (state == value)
					return;

				state = value;
				OnPropertyChanged();
			}
		}

		public DateTime FirstDiscovered { get; protected set; }

		public DateTime LastDiscovered
		{
			get => lastDiscovered;
			protected set
			{
				if (lastDiscovered == value)
					return;
				lastDiscovered = value;
				OnPropertyChanged();
			}
		}

		public HashSet<Port> Ports { get; private set; }

		public string OS => OSMatrix.BestMatchesToString();

		public OSMatrix OSMatrix
		{
			get => osMatrix;
			set
			{
				if (osMatrix == value)
					return;
				osMatrix = value;
				OnPropertyChanged();
			}
		}

		public IPInfo IPInfo
		{
			get => ipInfo;
			set
			{
				if (ipInfo == value)
					return;
				ipInfo = value;
				OnPropertyChanged();
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		private PhysicalAddress mac;
		private string hostname, ipv4, ipv6;
		private Vendor macVendor;
		private DeviceType type;
		private DeviceState state;
		private IPInfo ipInfo;
		private DateTime lastDiscovered;
		private OSMatrix osMatrix;

		public NetworkDevice()
		{
			Init();
		}

		public NetworkDevice(string ipOrHostname)
		{
			Init();

			if (IPAddress.TryParse(ipOrHostname, out var ip))
			{
				if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
					IPv4 = ip.ToString();
				else if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6)
					IPv6 = ip.ToString();
			}
			else
				Hostname = ipOrHostname;
		}

		public NetworkDevice(IPAddress ip)
		{
			Init();
			if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
				IPv4 = ip.ToString();
			else if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6)
				IPv6 = ip.ToString();
		}

		private void Init()
		{
			mac = PhysicalAddress.None;
			macVendor = Vendor.None;
			hostname = "";
			ipv4 = "";
			ipv6 = "";
			Type = DeviceType.Unknown;
			DeviceState = DeviceState.Offline;
			FirstDiscovered = LastDiscovered = DateTime.Now;

			ipInfo = IPInfo.None;
			OSMatrix = new OSMatrix();
			Ports = new HashSet<Port>();
		}

		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		public object Clone()
		{
			return MemberwiseClone();
		}

		public string GetHostIdentifier()
		{
			return Hostname == "" ? (IPv4 == "" ? IPv6 : IPv4) : Hostname;
		}

		public void Discovered()
		{
			LastDiscovered = DateTime.Now;
			DeviceState = DeviceState.Online;
		}

		public static DeviceType GetDeviceType(INetworkDevice device)
		{
			var nic = Core.Instance.CurrentInterface;
			var retVal = DeviceType.Unknown;

			if (device.MAC == PhysicalAddress.None)
				retVal = DeviceType.External;
			else if (nic != null)
				retVal = nic.IPv4 == device.IPv4 ? DeviceType.Current : nic.Network.Gateway.IPv4 == device.IPv4 ? DeviceType.Gateway : DeviceType.Internal;

			return retVal;
		}

		public static DeviceState GetApproxDeviceState(INetworkDevice device)
		{
			return device.LastDiscovered.AddSeconds(90) >= DateTime.Now ? DeviceState.Online : DeviceState.Offline;
		}

		#region Overrides
		public static bool operator !=(NetworkDevice device1, NetworkDevice device2)
		{
			return !(device1 == device2);
		}

		public static bool operator ==(NetworkDevice lhs, NetworkDevice rhs)
		{
			if (lhs is null)
			{
				if (rhs is null)
					return true;
				return false;
			}

			return lhs.Equals(rhs);
		}

		public override bool Equals(object obj)
		{
			var res = false;
			if (!(obj is null))
				if (obj is INetworkDevice device)
				{
					res = (IPv4.ToString() == device.IPv4.ToString() || IPv6.ToString() == device.IPv6.ToString()) && (MAC.ToString() == device.MAC.ToString());
				}
			return res;
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		public override string ToString()
		{
			return GetHostIdentifier() + " - " + MAC.ToCannonicalString();
		}

		public string ToPrintString()
		{
			var text = ipv4;

			if (ipv6 != "")
				text = text + "\n" + ipv6;

			if (mac != PhysicalAddress.None)
				text = text + "\n" + mac.ToCannonicalString();

			if (macVendor != Vendor.None)
				text = text + "\n" + macVendor.Manufacturer;

			if (OS != "")
				text = text + "\n" + OS;

			return text;
		}
		#endregion
	}
}
