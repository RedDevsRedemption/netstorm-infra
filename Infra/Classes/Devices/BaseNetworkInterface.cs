﻿using NetStorm.Classes.Location;
using NetStorm.Classes.NetworkTools;
using NetStorm.Classes.SendQueue;
using NetStorm.Modules.HostDiscovery;
using PacketDotNet;
using System;
using System.Net;
using System.Net.NetworkInformation;

namespace NetStorm.Classes.Devices
{
	abstract class BaseNetworkInterface : NetworkDevice, INetworkInterface
	{
		private const string ipinfodomain = "www.ipinfo.io";
		private const string ipinfosite = "http://ipinfo.io/ip";

		public new DeviceType Type { get => DeviceType.Current; set { } }
		public new string Hostname { get => Environment.MachineName; set { } }
		public new IPInfo IPInfo
		{
			get => ipInfo;
			set
			{
				if (value == IPInfo.None && ipInfo != null)
					return;

				ipInfo = value;
				OnPropertyChanged();
			}
		}

		public string InterfaceName { get; }
		public string LinkLocal { get; }
		public string ID { get; }

		public bool IsCaputring { get; protected set; }
		public Network Network { get; private set; }

		public string PublicIP
		{
			get
			{
				lock (publicIP)
					return publicIP;
			}
		}

		public event EventHandler<Packet> PacketCaptured;
		public event EventHandler CaptureStarted;
		public event EventHandler CaptureStopped;

		private string publicIP = "";
		private IPInfo ipInfo;

		public BaseNetworkInterface(string id,
			Gateway gateway,
			PhysicalAddress mac,
			string interfaceName,
			string ipv4,
			string netmaskv4,
			string ipv6)
		{
			ID = id;
			IPv4 = ipv4;
			IPv6 = ipv6;
			IPv6 = ipv6;
			InterfaceName = interfaceName;
			MAC = mac;
			publicIP = "";
			ipInfo = IPInfo.None;

			var cidrv4 = IPNetwork.ToCidr(IPAddress.Parse(netmaskv4));
			Network = new Network(IPNetwork.Parse(ipv4, cidrv4), gateway);
		}

		public BaseNetworkInterface(string id,
			Gateway gateway,
			PhysicalAddress mac,
			string interfaceName,
			string ipv4,
			string netmaskv4,
			string ipv6,
			string publicIP)
		{
			ID = id;
			IPv4 = ipv4;
			IPv6 = ipv6;
			InterfaceName = interfaceName;
			MAC = mac;
			ipInfo = IPInfo.None;
			this.publicIP = publicIP;

			var cidrv4 = IPNetwork.ToCidr(IPAddress.Parse(netmaskv4));
			Network = new Network(IPNetwork.Parse(ipv4, cidrv4), gateway);
		}

		public BaseNetworkInterface(INetworkInterface networkInterface)
		{
			ID = networkInterface.ID;
			InterfaceName = networkInterface.InterfaceName;

			Network = networkInterface.Network;
			IPv4 = networkInterface.IPv4;
			IPv6 = networkInterface.IPv6;
			MAC = networkInterface.MAC;
			LinkLocal = networkInterface.LinkLocal;
			ipInfo = networkInterface.IPInfo;
			publicIP = networkInterface.PublicIP;
		}

		public void RetrieveIPInfo()
		{
			publicIP = RetrievePublicIP();
			ipInfo = new IPLocater().GetIPInfo(publicIP);
		}

		public string RetrievePublicIP()
		{
			var wh = new WebHelper();
			var http = wh.Get(ipinfodomain, ipinfosite, IPAddress.Parse(IPv4));
			var res = "";

			if (!IPAddress.TryParse(http.Content, out var ip))
				res = IPAddress.Any.ToString();
			else res = ip.ToString();

			lock (publicIP)
				publicIP = res;

			Logger.Log.Debug($"Public ip: {res}");

			return res;
		}

		private void OnCaptureStarted()
		{
			IsCaputring = true;
			CaptureStarted?.Invoke(this, new EventArgs());
		}

		private void OnCaptureStopped()
		{
			IsCaputring = false;
			CaptureStopped?.Invoke(this, new EventArgs());
		}

		protected void OnPacketCaptured(Packet p)
		{
			PacketCaptured?.Invoke(this, p);
		}

		public void StartCapture()
		{
			if (IsCaputring)
				return;

			Logger.Log.Normal($"Starting capture on {InterfaceName}");
			StartCaptureInternal();
			OnCaptureStarted();
		}

		public void StopCapture()
		{
			if (!IsCaputring)
				return;

			StopCaptureInternal();
			OnCaptureStopped();
			Logger.Log.Normal($"Capture stopped on {InterfaceName}");
		}

		public abstract void Transmit(Packet packet);
		public abstract void Transmit(ISendQueue sendQueue);
		protected abstract void StartCaptureInternal();
		protected abstract void StopCaptureInternal();

		#region Overrides

		public override string ToString()
		{
			return $"{ InterfaceName } ({IPv4}/{Network.IPNetwork.Cidr})";
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		public override bool Equals(object obj)
		{
			var res = false;
			if (!(obj is null))
				if (obj is INetworkInterface iface)
					if (iface.ID == ID)
						res = true;
			return res;
		}

		public static bool operator ==(BaseNetworkInterface lhs, BaseNetworkInterface rhs)
		{
			if (lhs is null)
			{
				if (rhs is null)
					return true;
				return false;
			}

			return lhs.Equals(rhs);
		}

		public static bool operator !=(BaseNetworkInterface lhs, BaseNetworkInterface rhs)
		{
			return !(lhs == rhs);
		}

		#endregion
	}
}
