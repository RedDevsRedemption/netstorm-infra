﻿using NetStorm.Classes.SendQueue;
using PacketDotNet;
using SharpPcap.LibPcap;
using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace NetStorm.Classes.Devices
{
	class NetworkInterface : BaseNetworkInterface, IDisposable
	{
		private Socket ipv4Sock;
		private byte[] byteDataV4 = new byte[4096];
		private byte[] byteDataV6 = new byte[65575];

		public NetworkInterface(string id,
			Gateway gateway,
			PhysicalAddress mac,
			string interfaceName,
			string ipv4,
			string cidrv4,
			string ipv6) : base(id, gateway, mac, interfaceName, ipv4, cidrv4, ipv6)
		{
		}

		public NetworkInterface(
			string id,
			Gateway gateway,
			PhysicalAddress mac,
			string interfaceName,
			string ipv4,
			string cidrv4,
			string ipv6,
			string publicIP) : base(id, gateway, mac, interfaceName, ipv4, cidrv4, ipv6, publicIP)
		{
		}

		protected override void StartCaptureInternal()
		{
			//StartIPv4Socket();
		}

		protected override void StopCaptureInternal() { }

		protected void OnIPv4PacketReceived(IAsyncResult e)
		{
			try
			{
				var received = ipv4Sock.EndReceive(e);

				OnPacketCaptured(new IPv4Packet(new PacketDotNet.Utils.ByteArraySegment(byteDataV4)));

				byteDataV4 = new byte[4096];
				byteDataV6 = new byte[65575];

				if (IsCaputring)
					ipv4Sock.BeginReceive(byteDataV4, 0, byteDataV4.Length, SocketFlags.None,
						new AsyncCallback(OnIPv4PacketReceived), null);
			}
			catch { }
		}

		/// <summary>
		/// Prepare and start IPv4 socket
		/// </summary>
		protected void StartIPv4Socket()
		{
			byte[] byTrue = new byte[4] { 1, 0, 0, 0 };
			byte[] byOut = new byte[4];

			ipv4Sock = new Socket(AddressFamily.InterNetwork, SocketType.Raw, System.Net.Sockets.ProtocolType.IP);
			ipv4Sock.Bind(new IPEndPoint(IPAddress.Parse(IPv4), 0));
			ipv4Sock.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.HeaderIncluded, true);
			ipv4Sock.IOControl(IOControlCode.ReceiveAll, byTrue, byOut);
			ipv4Sock.BeginReceive(byteDataV4, 0, byteDataV4.Length, SocketFlags.None,
						new AsyncCallback(OnIPv4PacketReceived), null);
		}

		public override void Transmit(Packet packet)
		{
			if (packet is IPv4Packet ipv4)
				ipv4Sock.Send(packet.Bytes);
			else if (packet is IPv6Packet ipv6)
				return;
		}

		public override void Transmit(ISendQueue sendQueue)
		{
			foreach (var p in sendQueue)
				Transmit(p);
		}

		public void Dispose()
		{
			ipv4Sock?.Dispose();
		}
	}

	class PcapNetworkInterface : BaseNetworkInterface, IPcapNetworkInterface
	{
		public PcapDevice PcapDevice { get; }

		public PcapNetworkInterface(
			PcapDevice device,
			string id,
			Gateway gateway,
			PhysicalAddress mac,
			string interfaceName,
			string ipv4,
			string cidrv4,
			string ipv6,
			string publicIP) : base(id, gateway, mac, interfaceName, ipv4, cidrv4, ipv6, publicIP)
		{
			PcapDevice = device;
		}

		public PcapNetworkInterface(PcapDevice device, INetworkInterface networkInterface) : base(networkInterface)
		{
			PcapDevice = device;
		}

		protected override void StartCaptureInternal()
		{
			PcapDevice.Open(SharpPcap.DeviceMode.Promiscuous);
			PcapDevice.OnPacketArrival += PcapDevice_OnPacketArrival;
			PcapDevice.StartCapture();
		}

		protected override void StopCaptureInternal()
		{
			PcapDevice.StopCapture();
			PcapDevice.OnPacketArrival -= PcapDevice_OnPacketArrival;
			PcapDevice.Close();
		}

		private void PcapDevice_OnPacketArrival(object sender, SharpPcap.CaptureEventArgs e)
		{
			try
			{
				var ethPack = Packet.ParsePacket(LinkLayers.Ethernet, e.Packet.Data);
				if (ethPack != null)
					OnPacketCaptured(ethPack);
			}
			catch { }
		}

		public override void Transmit(Packet packet)
		{
			PcapDevice.SendPacket(packet);
		}

		public override void Transmit(ISendQueue sendQueue)
		{
			sendQueue.Transmit(this);
		}
	}
}