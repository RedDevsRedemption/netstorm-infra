﻿using NetStorm.Classes.SendQueue;
using PacketDotNet;
using SharpPcap.LibPcap;
using System;

namespace NetStorm.Classes.Devices
{
	interface INetworkInterface : INetworkDevice
	{
		string InterfaceName { get; }
		string ID { get; }
		string LinkLocal { get; }
		string PublicIP { get; }
		bool IsCaputring { get; }
		Network Network { get; }

		event EventHandler<Packet> PacketCaptured;
		event EventHandler CaptureStarted, CaptureStopped;

		/// <summary>
		/// Start packet capturing
		/// </summary>
		void StartCapture();

		/// <summary>
		/// Stop packet capturing
		/// </summary>
		void StopCapture();

		/// <summary>
		/// Transmit a packet
		/// </summary>
		/// <param name="packet"></param>
		void Transmit(Packet packet);

		/// <summary>
		/// Transmit a sendqueue
		/// </summary>
		/// <param name="sendQueue"></param>
		void Transmit(ISendQueue sendQueue);

		/// <summary>
		/// Send a GET request to retrieve the public IP
		/// </summary>
		/// <returns></returns>
		string RetrievePublicIP();

		/// <summary>
		/// Receive all ip info related parameters
		/// </summary>
		void RetrieveIPInfo();
	}

	interface IPcapNetworkInterface : INetworkInterface
	{
		PcapDevice PcapDevice { get; }
	}
}
