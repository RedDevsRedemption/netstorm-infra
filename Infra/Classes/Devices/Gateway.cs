﻿using System.Net;
using System.Net.NetworkInformation;

namespace NetStorm.Classes.Devices
{
	class Gateway : NetworkDevice
	{
		public new DeviceType Type => DeviceType.Gateway;
		public Gateway(IPAddress address, PhysicalAddress physicalAddress) : base(address)
		{
			MAC = physicalAddress;
		}

		public Gateway(INetworkDevice device)
		{
			IPv4 = device.IPv4;
			IPv6 = device.IPv6;
			Hostname = device.Hostname;
			OSMatrix = device.OSMatrix;
			FirstDiscovered = device.FirstDiscovered;
			LastDiscovered = device.LastDiscovered;
			MAC = device.MAC;
			IPInfo = device.IPInfo;
			DeviceState = device.DeviceState;

			foreach (var p in device.Ports)
				Ports.Add(p);
		}
	}
}
