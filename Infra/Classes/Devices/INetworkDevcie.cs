﻿using NetStorm.Classes.Location;
using NetStorm.Classes.OS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net.NetworkInformation;

namespace NetStorm.Classes.Devices
{
	enum DeviceState
	{
		Online,
		Offline,
	}

	enum DeviceType
	{
		Internal,
		External,
		Gateway,
		Current,
		Unknown
	}

	interface INetworkDevice : INotifyPropertyChanged, ICloneable
	{
		string IPv4 { get; set; }
		string IPv6 { get; set; }
		string Hostname { get; set; }
		DateTime FirstDiscovered { get; }
		DateTime LastDiscovered { get; }

		PhysicalAddress MAC { get; set; }
		DeviceType Type { get; set; }
		DeviceState DeviceState { get; set; }
		Vendor MACVendor { get; }
		HashSet<Port> Ports { get; }
		IPInfo IPInfo { get; set; }
		OSMatrix OSMatrix { get; set; }
		string OS { get; }

		string ToPrintString();
		string GetHostIdentifier();
		void Discovered();
	}
}
