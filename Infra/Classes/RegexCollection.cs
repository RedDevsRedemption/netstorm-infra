﻿using System.Text.RegularExpressions;

namespace NetStorm.Classes
{
	static class RegexCollection
	{
		public static readonly Regex PhysicalMacRegex = new Regex("([0-9A-Fa-f]{2}[:-]?){5}([0-9A-Fa-f]{2})");
		public static readonly Regex IPv4Regex = new Regex(@"((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)");
	}
}
