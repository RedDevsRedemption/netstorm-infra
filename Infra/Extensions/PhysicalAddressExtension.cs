﻿namespace System.Net.NetworkInformation
{
	public static class PhysicalAddressExtension
	{
		public static readonly PhysicalAddress Broadcast = PhysicalAddress.Parse("FFFFFFFFFFFF");
		public static readonly PhysicalAddress Null = PhysicalAddress.Parse("000000000000");

		public static string ToVendorString(this PhysicalAddress val)
		{
			var retVal = "";

			if (val.ToString().Length >= 6)
				retVal = val.ToString().Substring(0, 6);

			return retVal;
		}

		public static bool TryParse(string mac, out PhysicalAddress pmac)
		{
			PhysicalAddress a = PhysicalAddress.None;
			try
			{
				a = PhysicalAddress.Parse(mac.Replace(":", "-")
					.Replace("-", ".")
					.Replace(".", "_")
					.Replace("_", "")
					.ToUpper());
			}
			catch { }
			pmac = a;
			return a != PhysicalAddress.None;
		}

		public static string ToCannonicalString(this PhysicalAddress val)
		{
			var output = "";
			try
			{
				string m = val.ToString();

				for (int i = 0; i < m.Length; i++)
					if (i % 2 == 0 && i > 0)
						output += ":" + m[i];
					else
						output += m[i];
			}
			catch { return ""; }
			return output;
		}
	}
}
