﻿namespace NetStorm.Extensions
{
	public static class IntExtensions
	{
		public static bool Between(this int me, int bottom, int top)
		{
			return bottom <= me && top >= me;
		}
	}
}
