﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace NetStorm.Extensions
{
	public static class RegexExtensions
	{
		public static List<string> GetMatches(this Regex regex, string data)
		{
			var matches = regex.Match(data);
			var m = new List<string>();

			if (matches.Captures.Count == 0)
				return m;

			if (matches.Groups.Count > 0)
				foreach (Group group in matches.Groups)
					foreach (Capture capture in group.Captures)
						m.Add(capture.Value);
			else
				foreach (Match capture in matches.Captures)
					m.Add(capture.Value);

			return m;
		}
	}
}
