﻿namespace System.Net
{
	public static class IPAddressExtensions
	{
		public readonly static IPNetwork ClassA = IPNetwork.Parse("10.0.0.0", 8);
		public readonly static IPNetwork ClassB = IPNetwork.Parse("172.16.0.0", 12);
		public readonly static IPNetwork ClassC = IPNetwork.Parse("192.168.0.0", 16);

		public static bool IsPrivate(this IPAddress address)
		{
			return ClassA.Contains(address) || ClassB.Contains(address) || ClassC.Contains(address);
		}
	}
}
