﻿using System.Net.NetworkInformation;
using System.Text.RegularExpressions;

namespace System
{
	public static class StringExtensions
	{
		public static PhysicalAddress ToPhysicalAddress(this string mac)
		{
			string[] removables = new string[] { ":", "-", ".", "_", " " };
			PhysicalAddress retVal = null;

			mac = mac.ToUpper();
			for (int i = 0; i < removables.Length; i++)
				mac = mac.Replace(removables[i], "");

			try { retVal = PhysicalAddress.Parse(mac); }
			catch { retVal = PhysicalAddress.None; }

			return retVal;
		}

		public static string[] Split(this string s, string @string)
		{
			@string = Regex.Escape(@string);
			return Regex.Split(s, @string);
		}

		public static string RemoveEmptyLines(this string input)
		{
			return Regex.Replace(input, @"^\s*$\n|\r", string.Empty, RegexOptions.Multiline).TrimEnd();
		}

		public static string RemoveWhitespace(this string str, string seperator = "")
		{
			return string.Join(seperator, str.Split(default(string[]), StringSplitOptions.RemoveEmptyEntries));
		}
	}
}
