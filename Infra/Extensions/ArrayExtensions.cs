﻿using NetStorm.Classes.Devices;
using System.Collections.Generic;
using System.Linq;

namespace System.ArrayExtensions
{
	static class ArrayExtensions
	{
		public static string[] RemoveEmptyEntries(this string[] input)
		{
			return input.Where((val) => val != string.Empty).ToArray();
		}

		public static IEnumerable<T[]> Chunk<T>(this IEnumerable<T> data, int chunkSize)
		{
			if (chunkSize >= data.Count() || chunkSize == 0)
				return new T[][] { data.ToArray() };

			int lenght = data.Count() / chunkSize;
			int rest = data.Count() % chunkSize;
			var res = new T[(int)(lenght + 0.4f)][];

			for (int i = 0; i < lenght; i++)
				res[i] = data.SubArray(chunkSize * i, chunkSize);

			return res;
		}

		public static T[] SubArray<T>(this IEnumerable<T> data, int index, int length)
		{
			T[] result = new T[length];
			int count = data.Count();

			if (index >= count || length >= count)
				return result;

			if (index + length >= data.Count())
				Array.Copy(data.ToArray(), index, result, 0, count - index);
			else
				Array.Copy(data.ToArray(), index, result, 0, length);

			return result;
		}

		public static bool Contains(this IEnumerable<INetworkDevice> networkDevices, INetworkDevice device)
		{
			foreach (var dev in networkDevices)
				if (dev.Equals(device))
					return true;
			return false;
		}

		public static void ForEach(this Array array, Action<Array, int[]> action)
		{
			if (array.LongLength == 0) return;
			ArrayTraverse walker = new ArrayTraverse(array);
			do action(array, walker.Position);
			while (walker.Step());
		}

		private class ArrayTraverse
		{
			public int[] Position;
			private readonly int[] maxLengths;

			public ArrayTraverse(Array array)
			{
				maxLengths = new int[array.Rank];
				for (int i = 0; i < array.Rank; ++i)
				{
					maxLengths[i] = array.GetLength(i) - 1;
				}
				Position = new int[array.Rank];
			}

			public bool Step()
			{
				for (int i = 0; i < Position.Length; ++i)
				{
					if (Position[i] < maxLengths[i])
					{
						Position[i]++;
						for (int j = 0; j < i; j++)
						{
							Position[j] = 0;
						}
						return true;
					}
				}
				return false;
			}
		}
	}
}
