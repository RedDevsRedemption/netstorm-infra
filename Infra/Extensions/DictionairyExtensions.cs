﻿using System.Collections.Generic;

namespace NetStorm.Extensions
{
	public static class DictionaryExtensions
	{
		public static bool TryAdd<TKey, TValue>(this IDictionary<TKey, TValue> dict, TKey key, TValue value)
		{
			try
			{
				dict.Add(key, value);
				return true;
			}
			catch { return false; }
		}

		public static void AddRange<TKey, TValue>(this IDictionary<TKey, TValue> dict, IDictionary<TKey, TValue> vals)
		{
			foreach (var kvp in vals)
				dict.Add(kvp.Key, kvp.Value);
		}
	}
}
