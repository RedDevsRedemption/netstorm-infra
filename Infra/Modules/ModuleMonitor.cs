﻿using NetStorm.Classes.Devices;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NetStorm.Modules
{
	class ModuleMonitor
	{
		private INetworkInterface nic;
		private readonly IBackgroundModule module;

		public ModuleMonitor(IBackgroundModule module)
		{
			this.module = module;
			Core.Instance.OnShutdown += (_, __) =>
			{
				StopAutorun();
				StopMonitor();
				module.Abort();
			};
		}

		public void StartAutorun()
		{
			Core.Instance.InterfaceStarted += Instance_InterfaceStarted;
		}

		public void StopAutorun()
		{
			Core.Instance.InterfaceStarted -= Instance_InterfaceStarted;
		}

		public void StartMonitor(INetworkInterface nic)
		{
			StopMonitor();
			this.nic = nic;
			nic.CaptureStopped += Nic_CaptureStopped;
		}

		public void StopMonitor()
		{
			if (nic == null)
				return;

			nic.CaptureStopped -= Nic_CaptureStopped;
		}

		private void Instance_InterfaceStarted(object sender, INetworkInterface e)
		{
			Task.Run(() =>
			{
				module.SetNIC(e);
				module.Start();
				StartMonitor(e);
			});
		}

		private void Nic_CaptureStopped(object sender, EventArgs e)
		{
			module.Abort();
		}
	}
}
