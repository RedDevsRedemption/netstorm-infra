﻿using NetStorm.Modules.NetworkDiscovery;
using NetStorm.Modules.Portscanner;

namespace NetStorm.Modules
{
	class StaticModuleCollection
	{
		public static INetworkScanner NetworkScanner { get; private set; }
		public static IPortScanner PortScanner { get; private set; }

		static StaticModuleCollection()
		{
			NetworkScanner = BaseNetworkScanner.GetEnvironmentScanner();
			PortScanner = BasePortScanner.GetEnvironmentPortScanner();
			Core.Instance.InterfaceStarted += Instance_InterfaceStarted;
		}

		private static void Instance_InterfaceStarted(object sender, Classes.Devices.INetworkInterface e)
		{
			NetworkScanner.SetNIC(e);
			PortScanner.SetNIC(e);
		}
	}
}
