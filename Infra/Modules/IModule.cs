﻿using NetStorm.Classes.BackgroundWorker;
using NetStorm.Classes.Devices;

namespace NetStorm.Modules
{
	public interface IModule
	{
		/// <summary>
		/// Check if the module is supported by the platform
		/// </summary>
		bool Supported { get; }
	}

	interface IBackgroundModule : IModule, IBackgroundWorker
	{
		void SetNIC(INetworkInterface nic);
	}
}
