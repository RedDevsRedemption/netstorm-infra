﻿using System.Net;
using NetStorm.Classes.Devices;
using NetStorm.Classes.Location;
using NetStorm.Classes.NetworkTools;

namespace NetStorm.Modules.HostDiscovery
{
	class IPLocater : BaseHostScanner
	{
		private const string URIPath = "http://ip-api.com/xml/";

		/// <summary>
		/// Get IPInfo for a specific device
		/// </summary>
		/// <param name="device"></param>
		/// <param name="ignorePreviousValue"></param>
		/// <returns></returns>
		public override INetworkDevice ScanDevice(INetworkDevice device, bool ignorePreviousValue)
		{
			if (device.IPInfo != IPInfo.None && !ignorePreviousValue)
				return device;

			if (IPAddress.TryParse(device.IPv4, out var ip) && ip.IsPrivate())
				return device;

			device.IPInfo = InternalLookup(device.GetHostIdentifier());
			return device;
		}

		/// <summary>
		/// Get IPInfo for a specific host
		/// </summary>
		/// <param name="host"></param>
		/// <returns></returns>
		public IPInfo GetIPInfo(string host)
		{
			var res = IPInfo.None;
			try
			{
				if (IPAddress.TryParse(host, out var ip))
					host = ip.ToString();
				else
					host = Dns.GetHostEntry(host).AddressList[0].ToString();

				res = InternalLookup(host);
			}
			catch { }
			return res;
		}

		/// <summary>
		/// Issues a GET-Request to http://ip-api.com/xml/
		/// </summary>
		/// <param name="host"></param>
		/// <returns></returns>
		private IPInfo InternalLookup(string host)
		{
			var lang = "?lang=" + Config.Instance.CurrentCulture.TwoLetterISOLanguageName;
			var res = new WebHelper().Get(URIPath + host + lang);
			return IPInfo.ParseXML(res.Content);
		}
	}
}
