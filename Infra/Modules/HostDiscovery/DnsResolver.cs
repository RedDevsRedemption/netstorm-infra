﻿using System.Net;
using System.Net.Sockets;
using NetStorm.Classes.Devices;

namespace NetStorm.Modules.HostDiscovery
{
	class DnsResolver : BaseHostScanner
	{
		/// <summary>
		/// Issues a DNS-Query to receive hostname and/or ip-addresses
		/// </summary>
		/// <param name="device"></param>
		/// <param name="ignorePreviousValue"></param>
		/// <returns></returns>
		public override INetworkDevice ScanDevice(INetworkDevice device, bool ignorePreviousValue)
		{
			if (!ignorePreviousValue && device.Hostname != "")
				return device;

			var lookup = Lookup(device.GetHostIdentifier());

			device.IPv4 = device.IPv4 == "" ? lookup.IPv4 : device.IPv4;
			device.IPv6 = device.IPv6 == "" ? lookup.IPv6 : device.IPv6;
			device.Hostname = device.Hostname == "" ? lookup.Hostname : device.Hostname;
			return device;
		}

		/// <summary>
		/// Issues a DNS-Query to receive hostname and/or ip-addresses
		/// </summary>
		/// <param name="host"></param>
		/// <param name="tryNo"></param>
		/// <returns></returns>
		private INetworkDevice Lookup(string host, int tryNo = 0)
		{
			if (tryNo > 3)
				return NetworkDevice.Empty;

			try
			{
				var entry = Dns.GetHostEntry(host);

				if (string.IsNullOrEmpty(entry.HostName))
					return Lookup(host, tryNo++);

				var ipv4 = GetIP(entry, AddressFamily.InterNetwork);
				var ipv6 = GetIP(entry, AddressFamily.InterNetworkV6);
				return new NetworkDevice
				{
					IPv4 = ipv4,
					IPv6 = ipv6,
					Hostname = entry.HostName
				};
			}
			catch { return NetworkDevice.Empty; }
		}

		/// <summary>
		/// Get IP from IPHostEntry
		/// </summary>
		/// <param name="entry"></param>
		/// <param name="addressFamily"></param>
		/// <returns></returns>
		private string GetIP(IPHostEntry entry, AddressFamily addressFamily)
		{
			foreach (var e in entry.AddressList)
				if (e.AddressFamily == addressFamily)
					return e.ToString();

			return "";
		}
	}
}
