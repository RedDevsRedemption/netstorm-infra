﻿using NetStorm.Classes.Devices;
using NetStorm.Classes.OS;
using NetStorm.Extensions;
using System.Net;
using System.Net.NetworkInformation;

namespace NetStorm.Modules.HostDiscovery
{
	class PingFingerprinter : BaseHostScanner
	{
		/// <summary>
		/// Perform a naiv ICMP-OS-Fingerprint on a host
		/// </summary>
		/// <param name="device"></param>
		/// <param name="ignorePreviousValue"></param>
		/// <returns></returns>
		public override INetworkDevice ScanDevice(INetworkDevice device, bool ignorePreviousValue = false)
		{
			if (!ignorePreviousValue && device.OSMatrix.BestMatches().Count > 0)
				return device;

			var reply = GetPingReply(device.GetHostIdentifier());
			var matrix = Evaluate(reply, device.OSMatrix, device.Hostname != "");
			device.OSMatrix = matrix;
			return device;
		}

		/// <summary>
		/// Perform a naiv ICMP-OS-Fingerprint on a host
		/// </summary>
		/// <param name="host"></param>
		/// <param name="matrix"></param>
		/// <returns></returns>
		public OSMatrix Fingerprint(string host, OSMatrix matrix)
		{
			var isHostname = !IPAddress.TryParse(host, out var ip);
			var reply = GetPingReply(host);
			return Evaluate(reply, matrix, isHostname);
		}

		private PingReply GetPingReply(string ip)
		{
			PingReply reply = null;
			try
			{
				using (var p = new Ping())
				{
					reply = p.Send(ip);
				}
			}
			catch { }
			return reply;
		}

		/// <summary>
		/// Evaluate possible OS
		/// </summary>
		/// <param name="reply"></param>
		/// <param name="matrix"></param>
		/// <param name="hasHostname"></param>
		/// <returns></returns>
		private OSMatrix Evaluate(PingReply reply, OSMatrix matrix, bool hasHostname)
		{
			if (reply == null || reply.Options == null)
				return matrix;

			int tolerance = 0;
			int ttl = reply.Options.Ttl;

			if (255.Between(ttl - tolerance, ttl + tolerance))
			{
				matrix["bsd"].Match++;
				matrix["linux"].Match++;
				matrix["macos"].Match++;
			}
			if (254.Between(ttl - tolerance, ttl + tolerance))
			{
				matrix["solaris"].Match++;
				matrix["aix"].Match++;
				matrix["cisco"].Match++;
			}
			if (128.Between(ttl - tolerance, ttl + tolerance))
			{
				matrix["win10"].Match++;
			}
			if (64.Between(ttl - tolerance, ttl + tolerance))
			{
				matrix["bsd"].Match++;
				matrix["linux"].Match++;
				matrix["macos"].Match++;
				matrix["android"].Match++;
			}
			if (60.Between(ttl - tolerance, ttl + tolerance))
			{
				matrix["macos"].Match++;
			}
			if (32.Between(ttl - tolerance, ttl + tolerance))
			{
				matrix["winwork"].Match++;
				matrix["win95"].Match++;
				matrix["winnt3"].Match++;
				matrix["winnt4"].Match++;
			}

			if (!hasHostname)
			{
				matrix["android"].Match++;
				matrix["cisco"].Match++;
				matrix["linux"].Match++;
				matrix["aix"].Match++;
				matrix["ios"].Match++;
				matrix["win10"].Match++;
			}

			return matrix;
		}
	}
}
