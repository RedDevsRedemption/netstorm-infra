﻿using NetStorm.Classes.Devices;
using System.Net.NetworkInformation;
using System.Threading.Tasks;

namespace NetStorm.Modules.HostDiscovery
{
	/// <summary>
	/// A host discovery module
	/// </summary>
	interface IHostScanner
	{
		INetworkDevice ScanDevice(string host);
		INetworkDevice ScanDevice(string host, PhysicalAddress physicalAddress);
		INetworkDevice ScanDevice(INetworkDevice device, bool ignorePreviousValue);

		Task<INetworkDevice> ScanDeviceAsync(string host);
		Task<INetworkDevice> ScanDeviceAsync(string host, PhysicalAddress physicalAddress);
		Task<INetworkDevice> ScanDeviceAsync(INetworkDevice device, bool ignorePreviuosValue);
	}
}
