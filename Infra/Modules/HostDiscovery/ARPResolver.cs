﻿using System.Net.NetworkInformation;
using NetStorm.Classes.Devices;
using NetStorm.Classes.NetworkTools;

namespace NetStorm.Modules.HostDiscovery
{
	class ARPResolver : BaseHostScanner
	{
		public override INetworkDevice ScanDevice(INetworkDevice device, bool ignorePreviousValue)
		{
			if (device.MAC != PhysicalAddress.None && !ignorePreviousValue)
				return device;

			var arp = new ArpPing();
			var reply = arp.Send(device.IPv4);
			if (reply.ArpStatus == ArpStatus.Success)
				device.MAC = reply.PhysicalAddress;
			return device;
		}
	}
}
