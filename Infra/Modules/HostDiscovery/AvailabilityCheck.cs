﻿using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using NetStorm.Classes.Devices;
using NetStorm.Classes.NetworkTools;

namespace NetStorm.Modules.HostDiscovery
{
	class AvailabilityCheck : BaseHostScanner
	{
		/// <summary>
		/// Perform device.Discovered() if host is up
		/// </summary>
		/// <param name="device"></param>
		/// <param name="ignorePreviousValue"></param>
		/// <returns></returns>
		public override INetworkDevice ScanDevice(INetworkDevice device, bool ignorePreviousValue)
		{
			if (IsAvailable(device.IPv4 != "" ? device.IPv4 : device.GetHostIdentifier()))
				device.Discovered();

			return device;
		}

		/// <summary>
		/// Check if the device is online
		/// </summary>
		/// <param name="hostOrIP"></param>
		/// <returns></returns>
		public bool IsAvailable(string hostOrIP)
		{
			IPAddress.TryParse(hostOrIP, out var ip);

			if (ArpLookup(hostOrIP))
				return true;

			if (PingLookup(hostOrIP))
				return true;

			if (DnsLookup(hostOrIP))
				return true;

			if (ip != IPAddress.None)
			{
				if (ConnectTcp(ip, 80))
					return true;
			}

			return false;
		}

		/// <summary>
		/// Creates a TCP connection to check if the host is up
		/// </summary>
		/// <param name="address"></param>
		/// <param name="port"></param>
		/// <returns></returns>
		private bool ConnectTcp(IPAddress address, ushort port)
		{
			using (var tcpClient = new TcpClient())
			{
				try
				{
					tcpClient.Connect(address, port);
					if (tcpClient.Connected)
					{
						tcpClient.Close();
						return true;
					}
				}
				catch { }
				return false;
			}
		}

		/// <summary>
		/// Queries a DNS-Request to check if the host is up
		/// </summary>
		/// <param name="host"></param>
		/// <returns></returns>
		private bool DnsLookup(string host)
		{
			try
			{
				var hostEntry = Dns.GetHostEntry(host);
				if (hostEntry.AddressList.Length > 0)
					return true;
			}
			catch { }
			return false;
		}

		/// <summary>
		/// Issues an ARP-Request to check if the host is up
		/// </summary>
		/// <param name="host"></param>
		/// <returns></returns>
		private bool ArpLookup(string host)
		{
			if (!IPAddress.TryParse(host, out var ip))
				return false;

			var reply = ArpPing.GetEnvironmentArpPing().Send(ip.ToString());
			return reply.ArpStatus == ArpStatus.Success;
		}

		/// <summary>
		/// Performs a simple ICMP ping to check if the host is up
		/// </summary>
		/// <param name="host"></param>
		/// <returns></returns>
		private bool PingLookup(string host)
		{
			try
			{
				using (var p = new Ping())
				{
					var pingReply = p.Send(host);
					return pingReply.Status == IPStatus.Success;
				}
			}
			catch { }
			return false;
		}
	}
}
