﻿using System.Net.NetworkInformation;
using System.Threading.Tasks;
using NetStorm.Classes.Devices;

namespace NetStorm.Modules.HostDiscovery
{
	abstract class BaseHostScanner : IHostScanner
	{
		/// <summary>
		/// Scan a host address
		/// </summary>
		/// <param name="host"></param>
		/// <returns></returns>
		public INetworkDevice ScanDevice(string host)
		{
			return ScanDevice(new NetworkDevice(host), true);
		}

		/// <summary>
		/// Scan a host with its MAC-Address
		/// </summary>
		/// <param name="host"></param>
		/// <param name="physicalAddress"></param>
		/// <returns></returns>
		public INetworkDevice ScanDevice(string host, PhysicalAddress physicalAddress)
		{
			return ScanDevice(new NetworkDevice(host)
			{
				MAC = physicalAddress
			}, true);
		}

		/// <summary>
		/// Scan and modify the device during the scan (only if ignorePreviousValue = true)
		/// </summary>
		/// <param name="device"></param>
		/// <param name="ignorePreviousValue"></param>
		/// <returns></returns>
		public abstract INetworkDevice ScanDevice(INetworkDevice device, bool ignorePreviousValue);

		public Task<INetworkDevice> ScanDeviceAsync(string host)
		{
			return Task.Run(() => ScanDevice(host));
		}

		public Task<INetworkDevice> ScanDeviceAsync(string host, PhysicalAddress physicalAddress)
		{
			return Task.Run(() => ScanDevice(host, physicalAddress));
		}

		public Task<INetworkDevice> ScanDeviceAsync(INetworkDevice device, bool ignorePreviuosValue)
		{
			return Task.Run(() => ScanDevice(device, ignorePreviuosValue));
		}
	}
}
