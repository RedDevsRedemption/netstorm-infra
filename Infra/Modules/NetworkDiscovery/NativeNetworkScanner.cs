﻿using System;
using System.ArrayExtensions;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using NetStorm.Classes.BackgroundWorker;
using NetStorm.Classes.Devices;
using NetStorm.Classes.Lists;
using NetStorm.Classes.NetworkTools;

namespace NetStorm.Modules.NetworkDiscovery
{
	class NativeNetworkScanner : BaseNetworkScanner
	{
		public const int ScanIntervalMS = 60000;
		private const int MaxTasks = 128;

		private TimerWorker backgroundWorker;

		public override bool Supported => true;

		public NativeNetworkScanner() : base("Native Network Scanner")
		{
			backgroundWorker = new TimerWorker($"{Name} Timer")
			{
				IntervalMS = ScanIntervalMS
			};
			backgroundWorker.DoWork += (_, __) => PerformScan();
			backgroundWorker.PropertyChanged += (_, __) => OnPropertyChanged(__.PropertyName);
			backgroundWorker.Exit += (_, __) => OnExit();
		}

		/// <summary>
		/// Scan the network
		/// </summary>
		private void PerformScan()
		{
			var addressList = new RandomList<IPAddress>();
			addressList.AddRange(Network.IPNetwork.ListIPAddress());
			addressList.Shuffle();

			if (UseThreads)
				ThreadScan(addressList);
			else
				TaskScan(addressList);

			if (!RealtimeScan)
				Abort();
		}

		/// <summary>
		/// Perform a thread scan (faster, higher CPU usage)
		/// </summary>
		/// <param name="addresses"></param>
		private void ThreadScan(List<IPAddress> addresses)
		{
			var threadlist = new List<Thread>(MaxTasks);
			var chunkedList = addresses.Chunk(addresses.Count / MaxTasks).ToArray();

			Thread t;
			for (int i = 0; i < chunkedList.Length; i++)
			{
				var chunk = chunkedList[i];
				t = new Thread(() =>
				{
					for (int k = 0; k < chunk.Length; k++)
						IPLookup(chunk[k]);
				});
				t.Start();
				threadlist.Add(t);
			}

			foreach (var e in threadlist)
				e.Join();
		}

		/// <summary>
		/// Perform task scan (slower, less CPU usage)
		/// </summary>
		/// <param name="addresses"></param>
		private void TaskScan(List<IPAddress> addresses)
		{
			var tasklist = new List<Task>(MaxTasks);
			var chunkedList = addresses.Chunk(addresses.Count / MaxTasks).ToArray();

			for (int i = 0; i < chunkedList.Length; i++)
			{
				var chunk = chunkedList[i];
				tasklist.Add(Task.Run(() =>
				{
					for (int k = 0; k < chunk.Length; k++)
						IPLookup(chunk[k]);
				}));
			}

			foreach (var t in tasklist)
				t.Wait();
		}

		/// <summary>
		/// Scan an address
		/// </summary>
		/// <param name="a"></param>
		private void IPLookup(IPAddress a)
		{
			var ping = ArpPing.GetEnvironmentArpPing();
			var reply = ping.Send(a.ToString());

			if (reply.ArpStatus != ArpStatus.Success ||
				Network.IPNetwork.Broadcast.Equals(a) ||
				!a.IsPrivate())
				return;

			var device = new NetworkDevice(a)
			{
				MAC = reply.PhysicalAddress
			};
			OnDeviceDetected(device);
		}

		protected override bool InternalScan()
		{
			return backgroundWorker.Start();
		}

		protected override void InternalAbort()
		{
			backgroundWorker?.Abort();
		}

		public override void WaitForExit()
		{
			backgroundWorker?.WaitForExit();
		}
	}
}
