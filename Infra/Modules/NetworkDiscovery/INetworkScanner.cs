﻿using NetStorm.Classes;
using NetStorm.Classes.Devices;
using NetStorm.Modules.HostDiscovery;
using System;

namespace NetStorm.Modules.NetworkDiscovery
{
	interface INetworkScanner : IBackgroundModule
	{
		bool RealtimeScan { get; set; }
		bool UseThreads { get; set; }
		Network Network { get; }
		event EventHandler<INetworkDevice> DeviceDetected;

		/// <summary>
		/// Add a host discovery module
		/// </summary>
		/// <param name="module"></param>
		/// <returns></returns>
		bool AddModule(IHostScanner module);

		/// <summary>
		/// Check if a host discovery module is present
		/// </summary>
		/// <param name="module"></param>
		/// <returns></returns>
		bool ContainsModule(Type module);

		/// <summary>
		/// Remove a host discovery module
		/// </summary>
		/// <param name="module"></param>
		/// <returns></returns>
		bool RemoveModule(Type module);

		/// <summary>
		/// Start the network scan on a specific network
		/// </summary>
		/// <param name="network"></param>
		/// <returns></returns>
		bool Start(Network network);
	}
}
