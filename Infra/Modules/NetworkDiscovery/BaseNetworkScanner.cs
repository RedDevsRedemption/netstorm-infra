﻿using NetStorm.Classes;
using NetStorm.Classes.BackgroundWorker;
using NetStorm.Classes.Devices;
using NetStorm.Classes.Lists;
using NetStorm.Modules.HostDiscovery;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace NetStorm.Modules.NetworkDiscovery
{
	abstract class BaseNetworkScanner : INetworkScanner
	{
		public static INetworkScanner GetEnvironmentScanner()
		{
			return Config.Instance.NetworkAPI != NetworkAPI.Native ? (INetworkScanner)new PCAPNetworkScanner() : new NativeNetworkScanner();
		}
		public bool RealtimeScan { get; set; }
		public bool UseThreads { get; set; }
		public bool Running
		{
			get => running;
			private set
			{
				if (running == value)
					return;
				running = value;
				OnPropertyChanged();
			}
		}

		public string Name { get; private set; }
		public Network Network { get; private set; }

		public event EventHandler<INetworkDevice> DeviceDetected;
		public event PropertyChangedEventHandler PropertyChanged;
		public event EventHandler Exit;

		protected INetworkInterface nic { get; private set; }
		public abstract bool Supported { get; }

		private readonly Dictionary<string, IHostScanner> modules;
		private bool running;
		private readonly DeviceList internalList;
		private readonly TimerWorker rtScanner;
		private readonly ModuleMonitor monitor;

		public BaseNetworkScanner(string name)
		{
			Name = name;
			RealtimeScan = UseThreads = false;

			internalList = new DeviceList();
			rtScanner = new TimerWorker("State checker", 15000);
			modules = new Dictionary<string, IHostScanner>();
			monitor = new ModuleMonitor(this);

			rtScanner.DoWork += CheckDeviceState;
		}

		/// <summary>
		/// Clean up and invoke Exit function
		/// </summary>
		protected void OnExit()
		{
			monitor.StopMonitor();
			GC.Collect();
			Running = false;
			Logger.Log.Normal($"Network scan finished: {Network.ToString()}");
			Exit?.Invoke(this, new EventArgs());
		}

		protected void OnPropertyChanged([CallerMemberName] string caller = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(caller));
		}

		private void CheckDeviceState(object sender, EventArgs e)
		{
			foreach (var d in internalList)
				if (d.Type != DeviceType.Current)
					d.DeviceState = NetworkDevice.GetApproxDeviceState(d);
		}

		/// <summary>
		/// Perform host discovery on available hosts
		/// </summary>
		/// <param name="device"></param>
		protected async void OnDeviceDetected(INetworkDevice device)
		{
			Task t = null;

			if (Network.Gateway.Equals(device))
				device = Network.Gateway;

			if (Network.Devices.Contains(device.IPv4))
			{
				var d = Network.Devices[device.IPv4];
				d.Type = NetworkDevice.GetDeviceType(device);
				d.Discovered();
			}
			else
				Network.Devices.Add(device);

			if (internalList.Contains(device.IPv4))
				device = internalList[device.IPv4];
			else if (internalList.Add(device))
			{
				DeviceDetected?.Invoke(this, device);
				t = Task.Run(() => ModuleScan(device));
			}

			device.Type = NetworkDevice.GetDeviceType(device);
			device.Discovered();

			if (t != null)
				await t;
		}

		/// <summary>
		/// Perform host discovery on all available hosts
		/// </summary>
		protected void CompleteModuleScan()
		{
			if (!Running)
				return;

			var devs = new List<INetworkDevice>((int)internalList.Count);
			devs.AddRange(internalList);

			foreach (var d in devs)
				if (!Running)
					return;
				else
					ModuleScan(d);
		}

		/// <summary>
		/// Perform host discovery on a specific host
		/// </summary>
		/// <param name="device"></param>
		protected void ModuleScan(INetworkDevice device)
		{
			lock (modules)
				foreach (var k in modules)
					device = k.Value.ScanDevice(device, false);

			device.Type = NetworkDevice.GetDeviceType(device);
			device.Discovered();
		}

		public void SetNIC(INetworkInterface nic)
		{
			if (this.nic == nic)
				return;

			var running = Running;
			Stop();

			this.nic = nic;

			if (running)
				Start();
		}

		/// <summary>
		/// Start the network scan
		/// </summary>
		/// <returns></returns>
		public bool Start()
		{
			if (nic != null)
				return Start(nic.Network);

			return false;
		}

		/// <summary>
		/// Scan a specific network
		/// </summary>
		/// <param name="network"></param>
		/// <returns></returns>
		public bool Start(Network network)
		{
			if (Running ||
				nic == null ||
				!nic.IsCaputring)
				return false;

			monitor.StartMonitor(nic);
			Network = network;
			internalList.Clear();
			if (Running = InternalScan())
				Logger.Log.Normal($"Starting network scan on: {Network.ToString()}");

			if (Running && RealtimeScan)
				rtScanner.Start();

			return Running;
		}

		/// <summary>
		/// Stop the network scan and wait for cleanup
		/// </summary>
		public void Stop()
		{
			Abort();
			WaitForExit();
		}

		/// <summary>
		/// Abort all running tasks
		/// </summary>
		public void Abort()
		{
			rtScanner.Abort();
			InternalAbort();
		}

		/// <summary>
		/// Add a host discovery module
		/// </summary>
		/// <param name="module"></param>
		/// <returns></returns>
		public bool AddModule(IHostScanner module)
		{
			var res = false;
			var key = module.GetType().Name;

			lock (modules)
				res = modules.TryAdd(key, module);

			if (res)
				Task.Run(() => CompleteModuleScan());

			return res;
		}

		/// <summary>
		/// Check uf host discovery module is used
		/// </summary>
		/// <param name="module"></param>
		/// <returns></returns>
		public bool ContainsModule(Type module)
		{
			var key = module.Name;
			return modules.ContainsKey(key);
		}

		/// <summary>
		/// Remove a host discovery module
		/// </summary>
		/// <param name="module"></param>
		/// <returns></returns>
		public bool RemoveModule(Type module)
		{
			lock (modules)
			{
				var key = module.Name;
				return modules.Remove(key);
			}
		}

		/// <summary>
		/// Wait until all tasks are finished. This will go on forever, as long RealtimeScan = true and not aborted
		/// </summary>
		public abstract void WaitForExit();

		/// <summary>
		/// Start the network scan
		/// </summary>
		/// <param name="nic"></param>
		/// <returns></returns>
		protected abstract bool InternalScan();

		/// <summary>
		/// Abort all running tasks
		/// </summary>
		protected abstract void InternalAbort();
	}
}
