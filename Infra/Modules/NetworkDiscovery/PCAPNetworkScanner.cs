﻿using NetStorm.Classes.BackgroundWorker;
using NetStorm.Classes.Devices;
using NetStorm.Classes.Lists;
using NetStorm.Classes.PacketFilter;
using NetStorm.Classes.PacketReceiver;
using NetStorm.Classes.SendQueue;
using PacketDotNet;
using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading;

namespace NetStorm.Modules.NetworkDiscovery
{
	class PCAPNetworkScanner : BaseNetworkScanner
	{
		private readonly BasicPacketReceiver backgroundReceiver;
		private readonly TimerWorker backgroundSender;

		public override bool Supported => Config.Instance.NetworkAPI != NetworkAPI.Native;

		public PCAPNetworkScanner() : base("PCAP Network scanner")
		{
			backgroundReceiver = new BasicPacketReceiver(HistoryMode.None);
			backgroundReceiver.AddPacketFilter(new ArpFilter());
			backgroundReceiver.DoWork += ProcessPackets;
			backgroundSender = new TimerWorker("Network Packet Sender")
			{
				IntervalMS = 30000
			};
			backgroundSender.DoWork += (_, __) => SendPackets();
			backgroundReceiver.Exit += (_, __) => OnExit();
		}

		/// <summary>
		/// Raises OnDeviceDetected if packets are valid
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void ProcessPackets(object sender, IEnumerable<Packet> e)
		{
			foreach (EthernetPacket p in e)
			{
				if (((ArpPacket)p.PayloadPacket).Operation != ArpOperation.Response ||
					!((ArpPacket)p.PayloadPacket).SenderProtocolAddress.IsPrivate())
					continue;

				OnDeviceDetected(
					new NetworkDevice(((ArpPacket)p.PayloadPacket).SenderProtocolAddress)
					{
						MAC = ((ArpPacket)p.PayloadPacket).SenderHardwareAddress
					}
				);
			}
		}

		/// <summary>
		/// Sends all ARP-Packets 3 times
		/// </summary>
		private void SendPackets()
		{
			var list = new RandomList<IPAddress>();
			list.AddRange(Network.IPNetwork.ListIPAddress());
			list.Shuffle();

			using (var sendQueue = new SendQueue())
			{
				sendQueue.AddRange(GenerateArpRequests(list.ToArray()));
				for (int i = 0; i < 3; i++)
				{
					sendQueue.Transmit(nic);
					Thread.Sleep(1000);
				}
			}

			if (!RealtimeScan)
			{
				Thread.Sleep(10000);
				Abort();
			}
		}

		/// <summary>
		/// Generates ARP-Requests for all possible hosts per network
		/// </summary>
		/// <param name="addresses"></param>
		/// <returns></returns>
		private List<Packet> GenerateArpRequests(IPAddress[] addresses)
		{
			var list = new List<Packet>();
			EthernetPacket eth;
			ArpPacket arp;

			if (nic == null)
				return list;

			var senderIP = IPAddress.Parse(nic.IPv4);
			foreach (var ip in addresses)
			{
				if (Network.IPNetwork.Broadcast.ToString() == ip.ToString())
					continue;

				eth = new EthernetPacket(nic.MAC, PhysicalAddressExtension.Broadcast, EthernetType.Arp);
				arp = new ArpPacket(ArpOperation.Request, PhysicalAddressExtension.Broadcast, ip, nic.MAC, senderIP);
				eth.PayloadPacket = arp;
				eth.UpdateCalculatedValues();
				list.Add(eth);
			}
			return list;
		}

		protected override void InternalAbort()
		{
			backgroundSender?.Abort();
			backgroundReceiver?.Abort();
		}

		protected override bool InternalScan()
		{
			backgroundReceiver.Start(nic);
			backgroundSender.Start();
			return true;
		}

		public override void WaitForExit()
		{
			backgroundReceiver?.WaitForExit();
			backgroundSender?.WaitForExit();
		}
	}
}
