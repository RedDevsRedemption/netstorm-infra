﻿using NetStorm.Classes;
using NetStorm.Classes.BackgroundWorker;
using NetStorm.Classes.Banner;
using NetStorm.Classes.Devices;
using NetStorm.Classes.NetworkTools;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;

namespace NetStorm.Modules.Portscanner
{
	abstract class BasePortScanner : IPortScanner
	{
		public static IPortScanner GetEnvironmentPortScanner()
		{
			return Config.Instance.NetworkAPI == NetworkAPI.Native ? new NativePortScanner() : (IPortScanner)new PcapPortScanner();
		}

		private static readonly ushort[] BannerPorts = new ushort[]
		{
			80,
			443,
			1900
		};

		public string Name { get; private set; }
		public abstract PortscanMode ScanMode { get; set; }
		public bool UseThreads { get; set; }
		public bool GrabBanners { get; set; }
		public bool Running
		{
			get => running;
			private set
			{
				if (running == value)
					return;

				running = value;
				OnPropertyChanged();
			}
		}

		public event EventHandler Exit;
		public event PropertyChangedEventHandler PropertyChanged;
		public event EventHandler<Port> PortDiscovered;

		public INetworkDevice Target { get; private set; }
		protected ushort[] Ports { get; private set; }
		public abstract bool Supported { get; }

		protected readonly ResetToken token;
		protected INetworkInterface nic;

		private readonly ModuleMonitor monitor;
		private readonly WebHelper webHelper;
		private bool running;

		public BasePortScanner(string name)
		{
			Name = name;
			webHelper = new WebHelper();
			token = new ResetToken();
			monitor = new ModuleMonitor(this);
		}

		protected void OnPropertyChanged([CallerMemberName] string caller = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(caller));
		}

		/// <summary>
		/// Clean up and invoke Exit function
		/// </summary>
		protected void OnExit()
		{
			monitor.StopMonitor();
			GC.Collect();
			Running = false;
			Logger.Log.Normal($"Portscan finished on: {Target.GetHostIdentifier()}, " +
				$"open ports: {Target.Ports.Where(p => p.PortStatus == PortStatus.Open).Count()}");
			Exit?.Invoke(this, new EventArgs());
		}

		/// <summary>
		/// Add port to target and perform bannergrabbing
		/// </summary>
		/// <param name="p"></param>
		protected void OnPortFound(Port p)
		{
			if (GrabBanners && p.PortStatus != PortStatus.Closed && token.CanContinue())
			{
				var banner = GetBanner(p.Number);
				if (banner != null)
					p.Banners.AddRange(banner);
			}

			try
			{
				lock (Target.Ports)
					Target.Ports.Add(p);
			}
			catch
			{
				Port a = null;
				lock (Target.Ports)
					for (int i = 0; i < Target.Ports.Count; i++)
					{
						a = Target.Ports.ElementAt(i);
						if (a == p)
						{
							a = p;
							break;
						}
					}
			}
			PortDiscovered?.Invoke(this, p);
		}

		/// <summary>
		/// Grab banners
		/// </summary>
		/// <param name="port"></param>
		/// <returns></returns>
		protected IBanner[] GetBanner(ushort port)
		{
			if (!BannerPorts.Contains(port))
				return null;

			List<IBanner> res = new List<IBanner>();
			var response = webHelper.Get(Target.IPv4, port);

			if (ServerBanner.IsImportant(response, out var sb))
				res.Add(sb);

			if (WWWAuthBanner.IsImportant(response, out var wAuth))
				res.Add(wAuth);

			if (TitleBanner.IsImportant(response, out var tb))
				res.Add(tb);

			return res.ToArray();
		}

		public bool Start()
		{
			if (Running ||
				nic == null ||
				!nic.IsCaputring)
				return false;

			if (Running = InternalStart())
			{
				monitor.StartMonitor(nic);
				Logger.Log.Normal($"Starting portscan on: {Target.GetHostIdentifier()}");
			}

			return Running;
		}

		/// <summary>
		/// Set a portscan target
		/// </summary>
		/// <param name="device"></param>
		/// <param name="ports"></param>
		/// <returns></returns>
		public bool SetTarget(INetworkDevice device, ushort[] ports)
		{
			if (Running ||
				device.IPv4 == "")
				return false;

			Target = device;
			Ports = ports;
			return true;
		}

		/// <summary>
		/// Stop portscan and wait for cleanup
		/// </summary>
		public void Stop()
		{
			Abort();
			WaitForExit();
			Running = false;
		}

		/// <summary>
		/// Abort all running tasks
		/// </summary>
		public abstract void Abort();

		/// <summary>
		/// Wait until all tasks are finished
		/// </summary>
		public abstract void WaitForExit();

		/// <summary>
		/// Start port scan internal
		/// </summary>
		/// <param name="nic"></param>
		/// <returns></returns>
		protected abstract bool InternalStart();

		public void SetNIC(INetworkInterface nic)
		{
			if (this.nic == nic)
				return;

			Stop();
			this.nic = nic;
		}
	}
}
