﻿using NetStorm.Classes;
using NetStorm.Modules.HostDiscovery;
using System.ArrayExtensions;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace NetStorm.Modules.Portscanner
{
	class NativePortScanner : BasePortScanner
	{
		private const int MaxTasks = 64;
		private Thread bgWorker;

		public override PortscanMode ScanMode { get => PortscanMode.Connect; set { } }

		public override bool Supported => true;

		public NativePortScanner() : base("Native Port Scanner")
		{
		}

		public override void Abort()
		{
			token.Reset();
		}

		public override void WaitForExit()
		{
			bgWorker?.Join();
		}

		protected override bool InternalStart()
		{
			if (!new AvailabilityCheck().IsAvailable(Target.GetHostIdentifier()))
				return false;

			token.Set();
			bgWorker = new Thread(DoWork)
			{
				Name = Name + " Worker"
			};
			bgWorker.Start();
			return true;
		}

		/// <summary>
		/// Start task or thread portscans
		/// </summary>
		private void DoWork()
		{
			var threadList = new List<Thread>(MaxTasks);
			var taskList = new List<Task>(MaxTasks);

			var chunks = Ports.Chunk(Ports.Length / MaxTasks).ToArray();

			Thread t = null;
			var ip = IPAddress.Parse(Target.IPv4);
			for (int i = 0; i < chunks.Length; i++)
			{
				var chunk = chunks[i];
				if (UseThreads)
				{
					t = new Thread(() => ScanPorts(chunk, ip));
					t.Start();
					threadList.Add(t);
				}
				else
					taskList.Add(Task.Run(() => ScanPorts(chunk, ip)));

				if (!token.CanContinue())
					break;
			}

			foreach (var th in threadList)
				th.Join();

			foreach (var ts in taskList)
				ts.Wait();

			OnExit();
		}

		/// <summary>
		/// Scan specified ports on target
		/// </summary>
		/// <param name="ports"></param>
		/// <param name="ip"></param>
		private void ScanPorts(ushort[] ports, IPAddress ip)
		{
			var portlist = new List<Port>(ports.Length);

			TcpClient client = new TcpClient();
			for (int i = 0; i < ports.Length; i++)
			{
				try
				{
					client.Connect(ip, ports[i]);
					if (client.Connected)
					{
						var port = Port.GetPort(ports[i], Protocol.Tcp, PortStatus.Open);

						//TOOD: Get Banner from network stream

						portlist.Add(port);
						client.Close();
						client.Dispose();
						client = new TcpClient();
					}
					else
						portlist.Add(Port.GetPort(ports[i], Protocol.Tcp, PortStatus.Closed));
				}
				catch { portlist.Add(Port.GetPort(ports[i], Protocol.Tcp, PortStatus.Closed)); }

				if (!token.CanContinue())
					break;

				OnPortFound(portlist.Last());
			}
		}
	}
}
