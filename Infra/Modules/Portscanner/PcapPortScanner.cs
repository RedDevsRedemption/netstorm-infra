﻿using System.ArrayExtensions;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using NetStorm.Classes;
using NetStorm.Classes.Lists;
using NetStorm.Classes.PacketReceiver;
using NetStorm.Classes.SendQueue;
using NetStorm.Modules.Portscanner.Modes;
using PacketDotNet;

namespace NetStorm.Modules.Portscanner
{
	class PcapPortScanner : BasePortScanner
	{
		private const int MaxPacketsPerTransmit = 1000;
		private const int SleepPerTransmit_MS = 7500;

		public override PortscanMode ScanMode
		{
			get => mode;
			set
			{
				if (mode == value && scanMethod != null)
					return;

				Stop();
				mode = value;

				if (scanMethod != null)
					scanMethod.PortFound -= (_, __) => OnPortFound(__);

				switch (mode)
				{
					case PortscanMode.Fin:
						scanMethod = new FinScan();
						break;
					case PortscanMode.Null:
						scanMethod = new NullScan();
						break;
					case PortscanMode.XMAS:
						scanMethod = new XmasScan();
						break;
					default:
						scanMethod = new SynScan();
						break;
				}
				scanMethod.PortFound += (_, __) => OnPortFound(__);
			}
		}

		public override bool Supported => Config.Instance.NetworkAPI != NetworkAPI.Native;

		private Thread bgSender;
		private PortscanMode mode;
		private IPortscanMode scanMethod;
		private readonly HashSet<ushort> processedPorts;

		public PcapPortScanner() : base("PCAP port scanner")
		{
			ScanMode = PortscanMode.Syn;

			processedPorts = new HashSet<ushort>();
		}

		protected new void OnPortFound(Port e)
		{
			if (processedPorts.Contains(e.Number))
				return;

			processedPorts.Add(e.Number);
			base.OnPortFound(e);
		}

		private void BackgroundReceiver_DoWork(object sender, IEnumerable<Packet> e)
		{
			var ret = scanMethod.ProcessPackets(e);
			Transmit(ret);
		}

		private void SendPackets()
		{
			var backgroundReceiver = new BasicPacketReceiver(HistoryMode.None);
			backgroundReceiver.DoWork += BackgroundReceiver_DoWork;
			backgroundReceiver.Start(nic);

			var rndList = new RandomList<Packet>();
			rndList.AddRange(scanMethod.GetPackets(Ports));
			rndList.Shuffle();

			var chunked = rndList.Chunk(MaxPacketsPerTransmit);

			for (int i = 0; i < 2; i++)
				foreach (var chunk in chunked)
				{
					Transmit(chunk);
					if (!token.CanContinue(SleepPerTransmit_MS))
						break;
				}

			backgroundReceiver.Abort();
			scanMethod.Validate(Ports, processedPorts.ToArray());
			OnExit();
		}

		private bool Transmit(IEnumerable<Packet> packets)
		{
			try
			{
				using (var queue = new SendQueue())
				{
					queue.AddRange(packets);
					queue.Transmit(Core.Instance.CurrentInterface);
				}
				return true;
			}
			catch { }
			return false;
		}

		protected override bool InternalStart()
		{
			if (Core.Instance.CurrentInterface == null ||
				!Core.Instance.CurrentInterface.IsCaputring ||
				scanMethod == null)
				return false;

			scanMethod.SetTarget(Target);
			processedPorts.Clear();

			token.Set();

			bgSender = new Thread(SendPackets);
			bgSender.Start();
			return true;
		}

		public override void Abort()
		{
			token.Reset();
		}

		public override void WaitForExit()
		{
			bgSender?.Join();
		}
	}
}
