﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using NetStorm.Classes;
using NetStorm.Classes.PacketFilter;
using PacketDotNet;

namespace NetStorm.Modules.Portscanner.Modes
{
	class NullScan : BasePortscanMode
	{
		private const ushort bindingPort = 4352;

		private readonly Random rnd;
		private readonly static TCPFilter tcpFilter = new TCPFilter();

		public NullScan() : base("NULL")
		{
			rnd = new Random();
		}

		public override Packet[] GetPackets(ushort[] ports)
		{
			var res = new Packet[ports.Length];
			for (int i = 0; i < ports.Length; i++)
				res[i] = GetPacket(ports[i]);
			return res;
		}

		private Packet GetPacket(ushort port)
		{
			var tcp = new TcpPacket(bindingPort, port);
			var ipPack = new IPv4Packet(IPAddress.Parse(Core.Instance.CurrentInterface.IPv4), IPAddress.Parse(Target.IPv4));
			var eth = new EthernetPacket(Core.Instance.CurrentInterface.MAC, NextHop, EthernetType.IPv4);
			
			tcp.SequenceNumber = (uint)rnd.Next(ushort.MaxValue);

			eth.PayloadPacket = ipPack;
			ipPack.PayloadPacket = tcp;

			tcp.ParentPacket = ipPack;
			ipPack.ParentPacket = eth;

			tcp.UpdateTcpChecksum();
			tcp.UpdateCalculatedValues();
			ipPack.UpdateIPChecksum();
			eth.UpdateCalculatedValues();
			return eth;
		}

		public override Packet[] ProcessPackets(IEnumerable<Packet> packets)
		{
			var pList = new List<Packet>();
			foreach (var packet in packets)
			{
				if (!(packet is EthernetPacket eth) ||
					!(eth.PayloadPacket is IPv4Packet ipPack) ||
					!(ipPack.PayloadPacket is TcpPacket tcpPack))
					continue;

				if (ipPack.SourceAddress.ToString() != Target.IPv4 || ipPack.DestinationAddress.ToString() != SourceAddress.ToString())
					continue;

				if (tcpPack.DestinationPort != bindingPort)
					continue;

				var port = Port.GetPort(tcpPack.SourcePort, Protocol.Tcp);
				port.PortStatus = tcpPack.Reset ? PortStatus.Closed : PortStatus.Unknown;

				OnPortFound(port);
			}
			return pList.ToArray();
		}

		public override void Validate(ushort[] allPorts, ushort[] processedPorts)
		{
			Port res = null;
			foreach (var port in allPorts)
				if (!processedPorts.Contains(port))
				{
					res = Port.GetPort(port, Protocol.Tcp);
					res.PortStatus = PortStatus.Open | PortStatus.Filtered;
					OnPortFound(res);
				}
		}
	}
}
