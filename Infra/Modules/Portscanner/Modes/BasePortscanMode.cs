﻿using NetStorm.Classes;
using NetStorm.Classes.Devices;
using NetStorm.Classes.NetworkTools;
using PacketDotNet;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;

namespace NetStorm.Modules.Portscanner.Modes
{
	abstract class BasePortscanMode : IPortscanMode
	{
		public event EventHandler<Port> PortFound;
		protected INetworkDevice Target { get; private set; } = NetworkDevice.Empty;
		protected PhysicalAddress NextHop { get; private set; } = PhysicalAddress.None;
		protected IPAddress SourceAddress { get; private set; } = IPAddress.None;

		public string Type { get; }

		protected BasePortscanMode(string type)
		{
			Type = type;
		}

		public void SetTarget(INetworkDevice target)
		{
			Target = target;
			if (target.MAC == PhysicalAddress.None)
			{
				if (Core.Instance.CurrentInterface.Network.IPNetwork.Contains(IPAddress.Parse(target.IPv4)))
				{
					var ping = new ArpPing();
					var res = ping.Send(target.IPv4);
					NextHop = res.ArpStatus == ArpStatus.Success ? res.PhysicalAddress : Core.Instance.CurrentInterface.Network.Gateway.MAC;
				}
				else
					NextHop = Core.Instance.CurrentInterface.Network.Gateway.MAC;
			}
			else
				NextHop = target.MAC;
			SourceAddress = Core.Instance.CurrentInterface != null ? IPAddress.Parse(Core.Instance.CurrentInterface.IPv4) : IPAddress.None;
		}

		protected void OnPortFound(Port p)
		{
			PortFound?.Invoke(this, p);
		}

		public abstract Packet[] GetPackets(ushort[] ports);
		public abstract Packet[] ProcessPackets(IEnumerable<Packet> packets);
		public abstract void Validate(ushort[] allPorts, ushort[] processedPorts);
	}
}
