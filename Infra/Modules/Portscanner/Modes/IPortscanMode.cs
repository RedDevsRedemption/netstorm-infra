﻿using NetStorm.Classes;
using NetStorm.Classes.Devices;
using PacketDotNet;
using System;
using System.Collections.Generic;

namespace NetStorm.Modules.Portscanner.Modes
{
	interface IPortscanMode
	{
		string Type { get; }
		event EventHandler<Port> PortFound;

		void SetTarget(INetworkDevice target);
		Packet[] GetPackets(ushort[] ports);
		Packet[] ProcessPackets(IEnumerable<Packet> packets);
		void Validate(ushort[] allPorts, ushort[] processedPorts);
	}
}
