﻿using System.Collections.Generic;

namespace NetStorm.Modules.Portscanner
{
	enum PortSelection
	{
		QuickScan = 0,
		CommonHTTP = 1,
		Microsoft = 2,
		SQL = 3,
		RemoteDesktop = 4,
		WellKnown = 5,
		Full = 6
	}

	class PortSelector
	{
		public static ushort[] GeneratePorts(PortSelection selection)
		{
			switch (selection)
			{
				case PortSelection.CommonHTTP:
					return HTTP;
				case PortSelection.Full:
					return GenerateRange(1, ushort.MaxValue);
				case PortSelection.Microsoft:
					return Microsoft;
				case PortSelection.RemoteDesktop:
					return RD;
				case PortSelection.SQL:
					return SQL;
				case PortSelection.WellKnown:
					return GenerateRange(1, 1024);
				default:
					return QuickScan();
			}
		}

		private static ushort[] QuickScan()
		{
			var ports = new List<ushort>
			{
				21,22,23,25,
				45,53,110,
				111,113,
				199,256,
				554,587,912,
				993,995,1720,1723,
				8888,10257
			};
			ports.AddRange(HTTP);
			ports.AddRange(SQL);
			ports.AddRange(Microsoft);
			ports.AddRange(RD);
			return ports.ToArray();
		}

		private static ushort[] GenerateRange(ushort start, ushort end)
		{
			var res = new ushort[end - start + 1];
			for (ushort u = start; u <= end; u++)
				res[u - start] = u;
			return res;
		}

		private static readonly ushort[] HTTP = new ushort[]
		{
			80,443,3124,3128,5800,7000,8008,8080,8081,9080,9443,11371,12443,16080
		};

		private static readonly ushort[] SQL = new ushort[]
		{
			1433,1434,3306,4333,5432,6432,7306,7307,9001,25565
		};

		private static readonly ushort[] Microsoft = new ushort[]
		{
			123,135,137,138,139,143,445,500,593,1025,1433,1434,1900,3372,3398,5000
		};

		private static readonly ushort[] RD = new ushort[]
		{
			3283,3389,5500,5800,5900
		};
	}
}
