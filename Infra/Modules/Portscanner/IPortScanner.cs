﻿using NetStorm.Classes;
using NetStorm.Classes.Devices;
using System;

namespace NetStorm.Modules.Portscanner
{
	public enum PortscanMode
	{
		Syn = 0,
		Fin = 1,
		Null = 2,
		XMAS = 3,
		Connect = 4
	}

	interface IPortScanner : IBackgroundModule
	{
		bool UseThreads { get; set; }
		bool GrabBanners { get; set; }
		INetworkDevice Target { get; }
		PortscanMode ScanMode { get; set; }
		event EventHandler<Port> PortDiscovered;
		bool SetTarget(INetworkDevice device, ushort[] ports);
	}
}
