using System;
using System.Diagnostics;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using NetStorm.Classes;
using NetStorm.Classes.Devices;
using NetStorm.Classes.PacketFilter;
using NetStorm.Classes.PacketReceiver;
using NetStorm.Extensions;
using NetStorm.Logger;
using NetStorm.Modules;
using NetStorm.Modules.HostDiscovery;
using NetStorm.Modules.Portscanner;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using NetStorm.Modules.NetworkDiscovery;
using System.Collections;
using System.Collections.Generic;

namespace NetStorm
{
	public class Program
	{
		public static void Main(string[] args)
		{
			
			Log.Attach(new DebugLog());

			//var wc = new WebHelper();
			//var res = wc.Get("ipinfo.io", "http://ipinfo.io/ip", 80);
			//Console.WriteLine(res);
			//return;

			if (!Init())
			{
				Log.Attach(new ConsoleLog());
				Log.Error("No interfaces found");
				Console.ReadKey();
				return;
			}

			while (true)
			{
				var selectedOP = SelectOperation();

				switch (selectedOP)
				{
					case 0:
						return;
					case 1:
						RunREST();
						return;
					case 2:
						SelectInterface();
						RunNetworkScan();
						Core.Instance.StopInterface();
						break;
					case 3:
						RunIPLookup();
						break;
					case 4:
						SelectInterface();
						RunPortScan();
						Core.Instance.StopInterface();
						break;
					case 5:
						Core.Instance.RefreshNICList();
						PrintInterfaces();
						break;
					case 6:
						SelectInterface();
						Investigate();
						Core.Instance.StopInterface();
						break;
				}
				WriteImportant("---");
			}
		}

		private static bool Init()
		{
			Config.Instance.Analyze();
			Core.Instance.RefreshNICList();
			return Core.Instance.Interfaces.Count <= 0 ? false : true;
		}

		private static void RunIPLookup()
		{
			var host = SelectHost();
			var locater = new IPLocater();
			var dnsResolver = new DnsResolver();
			var fingerprinter = new PingFingerprinter();
			var arpResolver = new ARPResolver();

			var device = arpResolver.ScanDevice(host);
			device = dnsResolver.ScanDevice(device,true);
			device = fingerprinter.ScanDevice(device, true);
			device = locater.ScanDevice(device, true);

			WriteImportant($"--- Target: {device.IPv4} ---");
			var outp = (device.Hostname == "" ? device.IPv4 : device.Hostname + "\n" + device.IPv4) + Environment.NewLine +
				$"{device.MAC.ToCannonicalString()}\n" + 
				$"{device.OSMatrix.BestMatchesToString()}\n\n" +
				$"ISP: {device.IPInfo.ISP}\n" + 
				$"Organisation: {device.IPInfo.Organisation}\n" +
				$"State: {device.IPInfo.Location.Country}\n" +
				$"Region: {device.IPInfo.Location.Region} ({device.IPInfo.Location.RegionCode})\n" +
				$"City: {device.IPInfo.Location.ZIP}, {device.IPInfo.Location.City}\n" +
				$"Coordinates: {device.IPInfo.Location.Coordinates.ToString()}";
			Console.WriteLine(outp);
		}

		private static void RunNetworkScan()
		{
			var scanner = ConfigureNetworkScanner();
			var nic = Core.Instance.CurrentInterface;

			scanner.DeviceDetected += PrintDevice;
			WriteImportant("--- Showing detected devices ---");

			var sw = new Stopwatch();
			sw.Start();

			scanner.SetNIC(nic);
			scanner.Start();
			scanner.WaitForExit();

			sw.Stop();

			scanner.DeviceDetected -= PrintDevice;
			WriteImportant($"--- Discovered ({scanner.Network.Devices.Count}) ---");

			foreach (var d in scanner.Network.Devices)
				PrintDevice(d);

			WriteImportant($"Network scan finished on {nic.Network.IPNetwork.ToString()} in {sw.ElapsedMilliseconds / 1000} seconds");
		}

		private static void PrintDevice(INetworkDevice e)
		{
			Console.WriteLine($"{e.IPv4.PadRight(20)}" +
				$"{e.Hostname.PadRight(30)}" +
				$"{e.MAC.ToCannonicalString().PadRight(20)}" +
				$"{e.MACVendor.Manufacturer.PadRight(40)}" +
				$"{e.OS}");
		}

		private static void PrintDevice(object sender, INetworkDevice e)
		{
			Console.WriteLine($"Host {e.IPv4} at {e.MAC.ToCannonicalString()} ({e.MACVendor.Manufacturer}) is up. Starting investigation...");
		}

		private static INetworkScanner ConfigureNetworkScanner()
		{
			var scanner = StaticModuleCollection.NetworkScanner;
			scanner.UseThreads = true;
			scanner.RealtimeScan = false;

			scanner.AddModule(new DnsResolver());
			scanner.AddModule(new PingFingerprinter());

			return scanner;
		}

		private static void RunPortScan()
		{
			ushort portCount = 1025;
			var host = SelectHost();
			var ps = StaticModuleCollection.PortScanner;
			var ports = new ushort[portCount];

			for (ushort i = 0; i < portCount; i++)
				ports[i] = i;
			
			//ports = new ushort[]{
			//	80,		//HTTP
			//	443,	//HTTPS
			//	1900	//UPnP
			//};

			ps.UseThreads = true;
			ps.GrabBanners = true;
			ps.ScanMode = PortscanMode.Syn;
			ps.PortDiscovered += PrintPort;

			WriteImportant($"--- Performing {ps.ScanMode.ToString().ToUpper()}-Scan ---");

			var sw = new Stopwatch();
			sw.Start();

			ps.SetNIC(Core.Instance.CurrentInterface);
			ps.SetTarget(new NetworkDevice(host), ports);
			ps.Start();
			ps.WaitForExit();
			sw.Stop();
			ps.PortDiscovered -= PrintPort;
			
			WriteImportant($"--- Scanned {ports.Length} ports ---");
			WriteImportant($"Portscan finished on {ps.Target?.GetHostIdentifier()} in {sw.ElapsedMilliseconds / 1000} seconds");
		}

		private static void RunREST()
		{
            Log.Attach(new ConsoleLog());
			
			var host = new WebHostBuilder()
						 .UseKestrel()
						 .UseStartup<Startup>()
						 .Build();

			host.Run();
		}

		private static void Investigate()
		{
			var pr = new BasicPacketReceiver();
			var cl = new ConsoleLog();
			pr.DoWork += (_,__) => Pr_DoWork(_,__,cl);
			pr.Start();
			Thread.Sleep(10000);
			pr.Stop();
		}

		private static void Pr_DoWork(object sender, IEnumerable<PacketDotNet.Packet> e, ConsoleLog cl)
		{
			foreach (var p in e)
				cl.Log(new LogEntry(".ctor", p.ToString(), LogPriority.Debug));
		}

		private static void PrintPort(object sender, Port e)
		{
			if (e.PortStatus != PortStatus.Closed)
				Console.WriteLine(e.ToString());
		}

		private static void PrintInterfaces()
		{
			Console.WriteLine("=== Available NICs ===");

			for (int i = 0; i < Core.Instance.Interfaces.Count; i++)
			{
				var nic = Core.Instance.Interfaces.ElementAt(i);
				Console.WriteLine($"[{i + 1}]: " + nic.ToString());
			}
		}


		private static void WriteImportant(string s)
		{
			Console.WriteLine();
			Console.WriteLine(s);
			Console.WriteLine();
		}

		private static int SelectInterface()
		{
			int selection = 0;
			PrintInterfaces();
			Console.WriteLine();
			Console.Write("Select a nic: ");
			if (!int.TryParse(Console.ReadLine(), out selection))
				return SelectInterface();

			selection--;
			if (selection >= Core.Instance.Interfaces.Count || selection < 0)
				return SelectInterface();

			Core.Instance.SelectNIC(selection);
			return selection;
		}

		private static int SelectOperation()
		{
			int op = -1;

			Console.WriteLine(@"
=== Available options ===
[1]: Run as REST
---
[2]: Run a network scan
[3]: Perform IP-Lookup
[4]: Perform portscan
[5]: Refresh NICs
[6]: Investigate Traffic (10sec)
[0]: Quit
");
			Console.Write("Choose your wish: ");
			if (!int.TryParse(Console.ReadLine(), out op))
				return SelectOperation();

			if(!op.Between(0, 6))
				return SelectOperation();

			return op;
		}

		private static string SelectHost()
		{
			Console.Write("Enter a host you want to scan (IP or Hostname): ");
			return Console.ReadLine();
		}
	}

	public class Startup
	{
		public Startup(IHostingEnvironment env)
		{
		}

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			// Add framework services.
			services.AddMvc().AddJsonOptions(options =>
			{
				//return json format with Camel Case
				options.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver();
			});
			services.AddRouting();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app)
		{
			app.UseMvc();
		}
	}
}
