FROM appsvc/dotnetcore

WORKDIR /app

COPY . /app

CMD ["dotnet", "run"]

CMD ["dotnet", "dev-certs https --trust"]