# NetStorm
NetStorm is a still in progress, cross platform network discovery tool written in C# .NET core.

Currently, there are three modes available:

* Network scanner
* Portscanner
* IP-Lookup

## Why NetStorm Infra?
You can use NetStorm in any terminal you want, but it can also be run as REST.

This will be used for an UI implementation with Electron and Angular. You can find the repository [here](https://bitbucket.org/RedDevsRedemption/netstorm-ux/src/master/).

## Getting Started
This instruction will get you a working copy of the project.

### Prequesites
To this state of the software you need the [.NET Core SDK](https://dotnet.microsoft.com/download) installed. Don't worry, we are working towards a self contained executable :)

### Running NetStorm
You need to restore all dependencies before you can continue.

1. Open the repo folder in your terminal of choice
2. Run `dotnet restore`

#### Development
To run this app in development, just use `dotnet run` and let the magic happen.

#### Publish as self contained
You can create a working executable for:

* Windows `dotnet publish -r win10-x64 --output bin/dist/win10`
* Linux `dotnet publish -r linux-x64 --output bin/dist/linux`
* OSX `dotnet publish -r osx.10.11-x64 --output bin/dist/osx`

Now there should be a folder containing your favourite version of NetStorm. Enjoy :)

## License

This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License (CC BY-NC-ND).

To view a copy of this license, visit [http://creativecommons.org/licenses/by-nc-nd/4.0/](http://creativecommons.org/licenses/by-nc-nd/4.0/) or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

## Third party software

### Libraries
* [IPNetwork2](https://github.com/lduchosal/ipnetwork)
* [SharpPcap](https://github.com/chmorgan/sharppcap)

### Network drivers
NetStorm strongly benefits from either npcap, winpcap or libpcap. Some features won't work without it.

You can find those drivers here:

* [npcap](https://nmap.org/npcap/)
* [winpcap](https://www.winpcap.org/)
* [libpcap](http://www.tcpdump.org/)